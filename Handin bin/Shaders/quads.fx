
Texture2D shaderTexture : register(t0);
SamplerState samplerState;

cbuffer MatrixBuffer : register(b4)
{
	float4x4 myTowWorld;
	float4x4 myViewMatrix;
	float4x4 myProjectionMatrix;
};

struct VertexInput
{
	float4 myPosition : POSITION;
	float mySize : SIZE;
	float myAlpha : ALPHA;
};

struct GeometryInput
{
	float4 myPosition : SV_POSITION;
	float mySize : SIZE;
	float myAlpha : ALPHA;
};

struct PixelInput
{
	float4 myPosition : SV_POSITION;
	float2 myTexCoord : TEXCOORD;
	float myAlpha : ALPHA;
};

struct PixelOutputData
{
	float4 myColor : SV_TARGET;
};

GeometryInput VertexShader_(VertexInput aInput)
{
	VertexInput output = (VertexInput)0;
	
	output.myPosition.xy = aInput.myPosition.xy;
	output.myPosition.z = 0.0f;
	output.myPosition.w = 1.0f;
	
	//output.myPosition = mul(myTowWorld, output.myPosition);
	//output.myPosition = mul(myViewMatrix, output.myPosition);
	output.mySize = aInput.mySize;
	output.myAlpha = aInput.myAlpha;
	
	return output;
}

PixelOutputData PixelShader_(PixelInput aInput)
{
	PixelOutputData output;
	
	float2 tex;
	tex.x = aInput.myTexCoord.x;
	tex.y = aInput.myTexCoord.y;
	
	float4 textureColor = shaderTexture.Sample(samplerState, tex);
	output.myColor = textureColor;
	
	return output;
}

[maxvertexcount(4)]
void GeometryShader_(point GeometryInput aInput[1], inout TriangleStream<PixelInput> aTriStream)
{
	const float4 offset[4] = 
	{
		{0, -aInput[0].mySize, 0.5f, 0.0f},
		{aInput[0].mySize, -aInput[0].mySize, 0.5f, 0.0f},
		{0, 0, 0.5f, 0.0f},
		{aInput[0].mySize, 0, 0.5f, 0.0f}
	};
	
	float2 uvCoordinates[4] =
	{
		{0,1},
		{1,1},
		{0,0},
		{1,0}
	};
	
	for (int i = 0; i < 4; ++i)
	{
		PixelInput vertex = (PixelInput)0;
		vertex.myPosition = aInput[0].myPosition + offset[i];
		//vertex.myPosition = mul(myProjectionMatrix, vertex.myPosition);
		
		vertex.myTexCoord = uvCoordinates[i];
		vertex.myAlpha = aInput[0].myAlpha;
		
		aTriStream.Append(vertex);
	}

	aTriStream.RestartStrip();
}