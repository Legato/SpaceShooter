struct VS_SKYBOX_INPUT
{
	float4 position : POSITION;
};

struct PS_SKYBOX_INPUT
{
	float4 position : SV_POSITION;
	float4 worldPos : TEXCOORD0;
};

cbuffer projection : register(b0)
{
	matrix projection;
};

cbuffer cameraPos : register(b1)
{
	matrix cameraPositionInv;
	float4 cameraPosVector;
}

cbuffer instanceData : register(b2)
{
	struct
	{
		float4x4 transformation;
	} instance;
	
}

#define MAX_NUM_LIGHTS 12
cbuffer directionalLights : register(b3)
{
	float4 ambient;
	float4 directionalLights[MAX_NUM_LIGHTS];
	float4 directionalColors[MAX_NUM_LIGHTS];
	uint numOfDirectional;
	uint numOfPointLights;
	float2 empty;

	struct
	{
		float4 position;
		float4 colour;
		float radius;
		float intensity;
	} pointLights[MAX_NUM_LIGHTS];
};