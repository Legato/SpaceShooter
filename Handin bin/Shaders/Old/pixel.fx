#include "common.fx"

#define TDiffuse 0
#define TSpecular 1
#define TAmbient 2
#define TEmissive 3
#define THeight 4
#define TNormal 5
#define TShininess 6
#define TOpacity 7
#define TDisplacement 8
#define TLightMap 9
#define TReflexion 10
#define TNumOfTextures 11

struct PS_INPUT
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float4 uv : TEXCOORD0;
	float4 normal : TEXCOORD1;
	float4 tangent : TEXCOORD2;
	float4 biNormal : TEXCOORD3;
	float4 viewDirection : TEXCOORD4;
	float4 worldPos : TEXCOORD5;
	float2 specularity : SPECULARITY;
};

Texture2D inTexture[TNumOfTextures] :register(t0);

TextureCube cubemap : register (t11);

SamplerState SampleType;

float3 Diffuse(PS_INPUT input, float3 diffuse, float3 normal)
{
	float3 color;

	for (uint i = 0; i < numOfDirectional; ++i)
	{
		float3 dir = (float3)(directionalLights[i]);
		float lightIntensity = saturate(dot(normal, normalize(dir)));

		if (lightIntensity > 0)
		{
			color += diffuse.rgb * saturate(saturate(directionalColors[i].rgb * lightIntensity.xxx));
		}
	}

	for (i = 0; i < numOfPointLights; ++i)
	{
		float3 difference = pointLights[i].position.xyz - input.worldPos.xyz;

		float distance = length(difference);

		difference = normalize(difference);

		float attenuation = saturate(1.f - ((distance * distance) / (pointLights[i].radius * pointLights[i].radius)));

		float lambert = saturate(dot(difference, normal));

		color += diffuse.rgb * pointLights[i].colour.rgb * lambert.xxx * attenuation.xxx * pointLights[i].intensity.xxx;
	}

	return saturate(color);
}

float3 Specular(PS_INPUT input, float3 normal)
{
	float3 color = float3(0.f, 0.f, 0.f);
	normal = input.normal.xyz;

	float specularPower = 10;//input.specularity.x;
	float3 lightDir;
	float3 reflection;
	float intensity = 1;

	float3 specularIntensity = inTexture[TSpecular].Sample(SampleType, input.uv.xy).xyz;

	for (uint i = 0; i < numOfDirectional; ++i)
	{
		lightDir = (float3)(directionalLights[i]);

		float lightIntensity = saturate(dot(normal, lightDir));


		if (lightIntensity > 0)
		{
			reflection = normalize(2 * lightIntensity * normal - lightDir);
			float3 specular = pow(saturate(dot(reflection, input.viewDirection.rgb)), specularPower) * intensity;
			specular = specular * specularIntensity;

			color += specular;
		}
	}
	
	float3 difference;
	for (i = 0; i < numOfPointLights; ++i)
	{

		float3 difference = pointLights[i].position.xyz - input.worldPos.xyz;

		float distance = length(difference);

		float3 lightDir = normalize(difference);

		float attenuation = saturate(1.f - ((distance) / (pointLights[i].radius)));


		if (attenuation > 0)
		{
			float lightIntensity = saturate(dot(normal, lightDir));

			if (lightIntensity > 0)
			{
				reflection = normalize(2 * lightIntensity * normal - lightDir);
				float3 specular = pow(saturate(dot(reflection, input.viewDirection.rgb)), specularPower) * intensity;
				specular = specular * specularIntensity;

				color += specular;
			}
		}
	}

	return saturate(color);
}

float3 Emissive(PS_INPUT input)
{
	return inTexture[TEmissive].Sample(SampleType, input.uv.xy).rgb;
}

float3 NormalFromMap(PS_INPUT input)
{
	float3 normal = inTexture[TNormal].Sample(SampleType, input.uv.xy).rgb;

	normal = normalize(normal * float3(2.f, 2.f, 2.f) - float3(1.f, 1.f, 1.f));

	float3x3 normalTransform = float3x3((float3)input.biNormal, (float3)input.tangent, (float3)input.normal);

	normal = mul(normal, normalTransform);

	return normal;
}

float3 LightFromTexture(PS_INPUT input)
{
	return inTexture[TLightMap].Sample(SampleType, input.uv.xy).xyz;
}

float4 Frag_Pos(PS_INPUT input) : SV_TARGET
{
	float3 diffuse = inTexture[TDiffuse].Sample(SampleType, input.uv.xy).rgb;
	float3 normal = NormalFromMap(input);

	float3 ambientDiffuse = diffuse.rgb * ambient.rgb;
	float3 directLight = Diffuse(input, diffuse, normal);
	float3 specular = Specular(input, normal);
	float3 emissive = Emissive(input);

	float3 resultColor = saturate(directLight.rgb + ambientDiffuse.rgb + specular.rgb + emissive.rgb)/* * LightFromTexture(input)*/;
	return float4(resultColor.rgb, 1.f);
}

float4 PixelShader_Skybox(PS_SKYBOX_INPUT input) : SV_TARGET
{
	float3 toPixel = normalize(input.worldPos.xyz - cameraPosVector.xyz);
	float3 ambientLight = cubemap.SampleLevel(SampleType, toPixel.xyz, 0).xyz;

	float4 output;
	output.xyz = ambientLight;
	output.a = 1.0f;
	return output;
}