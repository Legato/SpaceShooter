
struct VertexInput
{
    float4 Position : POSITION;
    float2 Tex : TEXCOORD;
};

struct PixelInput
{
    float4 Position : SV_Position;
    float2 Tex : TEXCOORD;
};
SamplerState testState;
Texture2D intermediateTexture : register(t0);

PixelInput VS_Fullscreen(VertexInput input)
{
    PixelInput output = (PixelInput) 0;
    output.Position = input.Position;
    output.Tex = input.Tex;

    return output;
}

float4 PS_Fullscreen(PixelInput input) : SV_Target
{
    float3 resource = intermediateTexture.Sample(testState, input.Tex).rgb;
    return float4(resource.rgb, 1.0f);
}