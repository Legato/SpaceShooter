﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography.X509Certificates;

public class ExportVector3
{
    public static ExportVector3 Create(Vector3 aRawVector)
    {
        ExportVector3 vector3 = new ExportVector3();
        vector3.x = aRawVector.x;
        vector3.y = aRawVector.y;
        vector3.z = aRawVector.z;

        return vector3;
    }

    public float x;
    public float y;
    public float z;
}
