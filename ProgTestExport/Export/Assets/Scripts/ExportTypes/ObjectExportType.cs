﻿using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;


[System.Serializable]
public class ObjectExportType : BaseComponentData
{
    public ObjectExportType()
    {
        componentType = "gameObject";
    }
    //Inherent object properties
    public string name;
    public string tag;
    public bool isActive;
    public int instanceId;
    public ExportMatrix4x4 transform = new ExportMatrix4x4();
    //Component properties (will try to make into array of export objects)
    public List<BaseComponentData> components = new List<BaseComponentData>();
    
}