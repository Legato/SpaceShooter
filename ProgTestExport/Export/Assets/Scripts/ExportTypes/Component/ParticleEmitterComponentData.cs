﻿using UnityEngine;
using System.Collections;

public class ParticleEmitterComponentData : BaseComponentData {
    public ParticleEmitterComponentData()
    {
        componentType = "particleComponent";
    }

    public string name;
}
