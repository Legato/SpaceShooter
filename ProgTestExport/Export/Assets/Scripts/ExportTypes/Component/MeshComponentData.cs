﻿using System;
using System.Runtime.Serialization;
using UnityEngine;

[Serializable]
public class MeshComponentData : BaseComponentData
{
    public MeshComponentData()
    {
        componentType = "mesh";
    }

    public MeshComponentData(string aMeshName) : this()
    {
        meshName = aMeshName;
    }
    [SerializeField]
    public string meshName;

    //public void GetObjectData(SerializationInfo info, StreamingContext context)
    //{
    //    base.GetObjectData(info, context);
    //    info.AddValue("meshName", meshName);
    //}
}
