﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColliderComponentData : BaseComponentData
{
    public ColliderComponentData()
    {
        componentType = "collider";
    }

    public class ColliderData
    {
        public ExportVector3 center;
        public float radius;
        public List<ColliderData> children = new List<ColliderData>();
    }

    public ColliderData colliderData = new ColliderData();
}
