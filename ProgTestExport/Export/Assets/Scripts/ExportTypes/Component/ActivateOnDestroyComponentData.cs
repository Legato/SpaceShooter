﻿using UnityEngine;
using System.Collections;

public class ActivateOnDestroyComponentData : BaseComponentData {
    public ActivateOnDestroyComponentData()
    {
        componentType = "activateOnDestroy";
    }

    public string componentToActivate;
    public int componentToActivateId;
}
