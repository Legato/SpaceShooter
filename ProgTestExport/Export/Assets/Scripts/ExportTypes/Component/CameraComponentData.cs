﻿using UnityEngine;
using System.Collections;

public class CameraComponentData : BaseComponentData
{
    public ExportMatrix4x4 projection;
    public float fov;
    public float near;
    public float far;
    public float aspect;

    public struct ViewPort 
    {
        public float height;
        public float width;
        public float x;
        public float y;
    } ;

    public ViewPort viewPort;

    public CameraComponentData()
    {
        componentType = "camera";
    }
}
