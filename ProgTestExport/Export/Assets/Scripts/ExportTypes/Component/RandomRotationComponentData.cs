﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class RandomRotationComponentData : BaseComponentData
{
    public RandomRotationComponentData()
    {
        componentType = "randomRotation";
    }

    public ExportVector3 minRotation;
    public ExportVector3 maxRotation;
}
