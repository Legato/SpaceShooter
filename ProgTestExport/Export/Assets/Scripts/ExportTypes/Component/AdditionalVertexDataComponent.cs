﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AdditionalVertexDataComponent : BaseComponentData
{
    public AdditionalVertexDataComponent()
    {
        componentType = "additionalVertexData";
    }

    public List<ExportVector4> VertexColors = new List<ExportVector4>();
}
