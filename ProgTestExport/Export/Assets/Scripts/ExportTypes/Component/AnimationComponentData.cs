﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;

public class AnimationComponentData : BaseComponentData
{
    public AnimationComponentData()
    {
        componentType = "animation";
    }

    public float duration;
    public BezierCubicSplineWalker.SpliWalkerMode mode;
    public bool lookForward;
    public bool close;
    public BezierSplineInfo spline;

    [System.Serializable]
    public class Trigger
    {
        public ExportVector3 center;
        public float radius;
    };

    public Trigger trigger = new Trigger();
}
