﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using UnityEngine;

[Serializable]
public class BaseComponentData
{
    protected string componentType = "base";

    public string type
    {
        get { return componentType;
            ;
        }
    }
}
