﻿using UnityEngine;
using System.Collections;

public class RotationComponentData : BaseComponentData {
    public RotationComponentData()
    {
        componentType = "rotation";
    }

    public RotationComponentData(Vector3 aRotation) : this()
    {
        rotationSpeed = ExportVector3.Create(aRotation * Mathf.Deg2Rad);
    }

    public ExportVector3 rotationSpeed;
}
