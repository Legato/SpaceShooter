﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LightComponentData : BaseComponentData {
    public LightComponentData()
    {
        componentType = "light";
    }

    public string lightType;
    public float lightRange;
    public ExportVector4 lightColor;
    public float lightIntensity;
}
