﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
public class CustomMesh : MonoBehaviour
{
    public enum eRasterizerState
    {
        Default,
        WireFrame,
        NoCull
    }

    public enum eBlendState
    {
        Default,
        Alpha,
        NoAlpha
    }

    public MeshFilter MeshFilter;
    
    public eRasterizerState RasterizerState;
    public eRasterizerState BlendState;
    void Start()
    {
        MeshFilter = GetComponent<MeshFilter>();
    }
}
