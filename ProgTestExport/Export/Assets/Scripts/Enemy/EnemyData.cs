﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class EnemyData : MonoBehaviour
{
    public short maxHp;
    public float speed;
    public float fireRate;
    public short damage;
    public float range;
    public Vector3 shootingOffset;
    public bool showGizmos = true;
    private short MaxHp
    {
        get
        {
            return maxHp;
        }
        set
        {
            if (maxHp < 0)
            {
                maxHp = 0;
            }

            
        }
    }

    private float Speed
    {
        get
        {
            return speed;
        }
        set
        {
            if (speed < 0)
            {
                speed = 0;
            }


        }
    }

    private float FireRate
    {
        get
        {
            return fireRate;
        }
        set
        {
            if (fireRate < 0)
            {
                fireRate = 0;
            }


        }
    }

    private short Damage
    {
        get
        {
            return damage;
        }
        set
        {
            if (damage < 0)
            {
                damage = 0;
            }


        }
    }

    void Update()
    {
        MaxHp = maxHp;
        Speed = speed;
    }

    public EnemyComponentData GetEnemyData()
    {
        EnemyComponentData exData = new EnemyComponentData();

        exData.maxHp = MaxHp;
        exData.speed = Speed;
        exData.fireRate = FireRate;
        exData.damage = Damage;
        exData.range = range;
        exData.shootingOffset = ExportVector3.Create(shootingOffset);

        return exData;
    }

    void OnDrawGizmos()
    {
        if (showGizmos == true)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, range);
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(transform.TransformPoint(shootingOffset), 0.1f);
        }
        
    }
}