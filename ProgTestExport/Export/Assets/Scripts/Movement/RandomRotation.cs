﻿using UnityEngine;
using System.Collections;

public class RandomRotation : MonoBehaviour
{

    public Vector3 minRotation;
    public Vector3 maxRotation;

    private Vector3 rotation;
	// Use this for initialization
	void Start () {
	    rotation = new Vector3(Random.Range(minRotation.x, maxRotation.x), Random.Range(minRotation.y, maxRotation.y), Random.Range(minRotation.z, maxRotation.z));
	}
	
	// Update is called once per frame
	void Update () {
	    transform.Rotate(rotation * Time.deltaTime);
	}

    public RandomRotationComponentData GetData()
    {
        RandomRotationComponentData data = new RandomRotationComponentData();
        data.minRotation = ExportVector3.Create(minRotation * Mathf.Deg2Rad);
        data.maxRotation = ExportVector3.Create(maxRotation * Mathf.Deg2Rad);

        return data;
    }
}
