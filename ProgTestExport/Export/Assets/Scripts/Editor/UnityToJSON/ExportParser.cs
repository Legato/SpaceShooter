﻿using UnityEngine;
using System.Collections;
using System;

public class ExportParser : MonoBehaviour
{
    public static AnimationComponentData Parse(BezierCubicSplineWalker component)
    {
        AnimationComponentData info = new AnimationComponentData();
        info.duration = component.duration;
        info.mode = component.mode;
        info.lookForward = component.lookForward;

        if (component.spline != null)
        {
            info.spline = component.spline.GetSplineInfo();
            info.close = component.spline.close;
        }
        else
        {
            Debug.Log("No spline attached to splinewalker.");
        }

        info.trigger.center = ExportVector3.Create(component.transform.TransformPoint(component.offset));
        info.trigger.radius = component.activationRadius;

        return info;
    }
}
