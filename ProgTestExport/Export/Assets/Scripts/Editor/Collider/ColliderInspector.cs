﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CustomEditor(typeof(NestedSphereCollider))]
public class ColliderInspector : Editor
{
    private NestedSphereCollider nestedSphereCollider;

    public override void OnInspectorGUI()
    {
        nestedSphereCollider = target as NestedSphereCollider;

        if (nestedSphereCollider.internalCollider != null)
        {
            EditorGUI.BeginChangeCheck();
            bool showSphere = EditorGUILayout.Toggle("Show colliders", nestedSphereCollider.showColliders);
            if (EditorGUI.EndChangeCheck() == true)
            {
                Undo.RecordObject(nestedSphereCollider, "Moved collider center");
                nestedSphereCollider.showColliders = showSphere;
                EditorUtility.SetDirty(nestedSphereCollider);
            }

            ShowColliderGUI(nestedSphereCollider.internalCollider, 0);
        }
    }

    void ShowColliderGUI(NestedSphereCollider.InternalCollider internalCollider, uint aLevel)
    {
        
        internalCollider.showContent = EditorGUILayout.Foldout(internalCollider.showContent, "Level " + aLevel);
        if (internalCollider.showContent == true)
        {
            if (aLevel > 0)
            {

                if (GUILayout.Button("X", GUILayout.Width(20)) == true)
                {
                    
                    Undo.RecordObject(nestedSphereCollider, "Removed collider child");
                    Debug.Log("Removed collider. Parent now has " + (internalCollider.parent.children.Count - 1) + " children.");
                    internalCollider.parent.children.Remove(internalCollider);
                    
                    EditorUtility.SetDirty(nestedSphereCollider);
                    Debug.Log("Removed collider.");
                    Repaint();
                    return;
                }
            }
            EditorGUI.BeginChangeCheck();
            bool showSphere = EditorGUILayout.Toggle("Show collider", internalCollider.showMeshSphere);
            if (EditorGUI.EndChangeCheck() == true)
            {
                Undo.RecordObject(nestedSphereCollider, "Moved collider center");
                internalCollider.showMeshSphere = showSphere;
                EditorUtility.SetDirty(nestedSphereCollider);
            }

            EditorGUI.BeginChangeCheck();
            Vector3 offset = (Vector3)EditorGUILayout.Vector3Field("Center", internalCollider.center);
            if (EditorGUI.EndChangeCheck() == true)
            {
                Undo.RecordObject(nestedSphereCollider, "Moved collider center");
                internalCollider.center = offset;
                EditorUtility.SetDirty(nestedSphereCollider);
            }

            EditorGUI.BeginChangeCheck();
            float radius = EditorGUILayout.FloatField("Radius", internalCollider.radius);
            if (EditorGUI.EndChangeCheck() == true)
            {
                Undo.RecordObject(nestedSphereCollider, "Changed collider radius");
                internalCollider.radius = radius;
                EditorUtility.SetDirty(nestedSphereCollider);
            }

            ++aLevel;
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space((1 + aLevel) * 10 );
            EditorGUILayout.BeginVertical();
            foreach (NestedSphereCollider.InternalCollider internalColliderChild in internalCollider.children)
            {
                if (internalColliderChild.parent == null)
                {
                    internalColliderChild.parent = internalCollider;
                }
                ShowColliderGUI(internalColliderChild, aLevel);
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();

            
            if (GUILayout.Button("Add child", GUILayout.Width(100)) == true)
            {

                Undo.RecordObject(nestedSphereCollider, "Added collider child");
                internalCollider.children.Add(new NestedSphereCollider.InternalCollider(internalCollider));
                EditorUtility.SetDirty(nestedSphereCollider);
                Debug.Log("Added child. Collider now has " + internalCollider.children.Count + " children.");
                Repaint();
            }
        }
        
    }
}
