﻿using System;
using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(CustomBehaviour))]
public class BehaviourEditor : Editor
{
    private CustomBehaviour customBehaviour;
    public override void OnInspectorGUI()
    {
        customBehaviour = target as CustomBehaviour;

        DrawBehaviourList();
        DrawAddBehaviour();
    }

    private void DrawAddBehaviour()
    {
        if (GUILayout.Button("Add behaviour") == true)
        {
            Undo.RecordObject(customBehaviour, "Removed collider child");
            Debug.Log("Removed behaviour.");
            BaseBehaviour behaviour = CreateBehaviour((CustomBehaviour.eBehaviourTypes) 0);

            customBehaviour.Add(behaviour);
            
            EditorUtility.SetDirty(customBehaviour);
        }
    }

    private BaseBehaviour CreateBehaviour(CustomBehaviour.eBehaviourTypes behaviourType)
    {
        BaseBehaviour behaviour = null;

        switch (behaviourType)
        {
            case CustomBehaviour.eBehaviourTypes.MoveTowards:
                behaviour = new MoveTowards();
                break;
            case CustomBehaviour.eBehaviourTypes.Avoid:
                behaviour = new Avoid();
                break;
            case CustomBehaviour.eBehaviourTypes.Wander:
                behaviour = new Wander();
                break;
            default:
                throw new ArgumentOutOfRangeException("behaviourType", behaviourType, null);
        }

        return behaviour;
    }

    private void DrawBehaviourList()
    {
        foreach (BaseBehaviour behaviour in customBehaviour.Behaviours)
        {
            EditorGUILayout.Separator();
            RemoveWidget(behaviour);

            BehaviourTypeWidget(behaviour);
            

            switch (behaviour.BehaviourType)
            {
                case CustomBehaviour.eBehaviourTypes.MoveTowards:
                    DrawBox((MoveTowards) behaviour);
                    break;
                case CustomBehaviour.eBehaviourTypes.Avoid:
                    DrawBox((Avoid)behaviour);
                    break;
                case CustomBehaviour.eBehaviourTypes.Wander:
                    DrawBox((Wander)behaviour);
                    break;   
                default:
                    throw new ArgumentOutOfRangeException();
            }

            WeightWidget(behaviour);
        }
    }

    private void DrawBox(Wander behaviour)
    {
        EditorGUI.BeginChangeCheck();
        float speed = EditorGUILayout.FloatField("Speed", behaviour.Speed);
        if (EditorGUI.EndChangeCheck() == true)
        {
            Undo.RecordObject(customBehaviour, "Changed movement speed");
            Debug.Log("Changed movement speed.");
            behaviour.Speed = speed;
            EditorUtility.SetDirty(customBehaviour);
        }
    }

    private void BehaviourTypeWidget(BaseBehaviour behaviour)
    {
        EditorGUI.BeginChangeCheck();


        CustomBehaviour.eBehaviourTypes type = (CustomBehaviour.eBehaviourTypes)EditorGUILayout.Popup("Type",(int)behaviour.BehaviourType, Enum.GetNames(typeof(CustomBehaviour.eBehaviourTypes)));
        if (EditorGUI.EndChangeCheck() == true)
        {
            Undo.RecordObject(customBehaviour, "Changed behaviour type");
            Debug.Log("Changed behaviour type");
            customBehaviour.Change(behaviour, CreateBehaviour(type));
            EditorUtility.SetDirty(customBehaviour);
        }
    }

    private void RemoveWidget(BaseBehaviour behaviour)
    {
        if (GUILayout.Button("X", GUILayout.Width(20)) == true)
        {
            Undo.RecordObject(customBehaviour, "Removed collider child");
            Debug.Log("Removed behaviour.");
            customBehaviour.Remove(behaviour);

            EditorUtility.SetDirty(customBehaviour);
            Repaint();
            return;
        }
    }

    private void WeightWidget(BaseBehaviour behaviour)
    {
        EditorGUI.BeginChangeCheck();
        float weight = EditorGUILayout.FloatField("Weight", behaviour.Weight);
        if (EditorGUI.EndChangeCheck() == true)
        {
            Undo.RecordObject(customBehaviour, "Changed behaviour weight");
            Debug.Log("Changed behaviour weight");
            behaviour.Weight = weight;
            EditorUtility.SetDirty(customBehaviour);
        }
    }

    private void DrawBox(Avoid behaviour)
    {
        EditorGUI.BeginChangeCheck();
        Transform transform = (Transform)EditorGUILayout.ObjectField("Target", behaviour.Target, typeof(Transform));
        if (EditorGUI.EndChangeCheck() == true)
        {
            Undo.RecordObject(customBehaviour, "Changed MoveTowards target.");
            Debug.Log("Changed MoveTowards target.");
            behaviour.Target = transform;
            EditorUtility.SetDirty(customBehaviour);
        }
    }

    private void DrawBox(MoveTowards behaviour)
    {
        EditorGUI.BeginChangeCheck();
        Transform transform = (Transform)EditorGUILayout.ObjectField("Target", behaviour.Target, typeof(Transform));
        if (EditorGUI.EndChangeCheck() == true)
        {
            Undo.RecordObject(customBehaviour, "Changed MoveTowards target.");
            Debug.Log("Changed MoveTowards target.");
            behaviour.Target = transform;
            EditorUtility.SetDirty(customBehaviour);
        }
        EditorGUI.BeginChangeCheck();
        float speed = EditorGUILayout.FloatField("Speed", behaviour.Speed);
        if (EditorGUI.EndChangeCheck() == true)
        {
            Undo.RecordObject(customBehaviour, "Changed movement speed");
            Debug.Log("Changed movement speed.");
            behaviour.Speed = speed;
            EditorUtility.SetDirty(customBehaviour);
        }
    }
}
