﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomBehaviour : MonoBehaviour
{
    public enum eBehaviourTypes
    {
        MoveTowards,
        Avoid,
        Wander
    }
    private List<BaseBehaviour> behaviours = new List<BaseBehaviour>();

    public List<BaseBehaviour> Behaviours
    {
        get { return behaviours;}
    }

    public void Remove(BaseBehaviour behaviour)
    {
        behaviours.Remove(behaviour);
    }

    public void Add(BaseBehaviour behaviour)
    {
        if (behaviour != null)
        {
            behaviours.Add(behaviour);
        }
    }

    public void Change(BaseBehaviour originalBehaviour, BaseBehaviour newBehaviour)
    {
        int originalIndex = behaviours.IndexOf(originalBehaviour);
        if (originalIndex < 0)
        {
            behaviours.Add(newBehaviour);
        }
        else
        {
            behaviours[originalIndex] = newBehaviour;
        }
    }
}
