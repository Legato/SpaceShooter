﻿using UnityEngine;
using System.Collections;

public abstract class BaseBehaviour
{
    protected CustomBehaviour.eBehaviourTypes behaviourType;

    public CustomBehaviour.eBehaviourTypes BehaviourType
    {
        get { return behaviourType; }
    }
    public float Weight = 0;

    
}
