﻿using UnityEngine;
using System.Collections;

public class MoveTowards : BaseBehaviour
{
    public MoveTowards()
    {
        behaviourType = CustomBehaviour.eBehaviourTypes.MoveTowards;
    }
    public Transform Target;
    private float speed;

    public float Speed
    {
        get { return speed; }
        set
        {
            if (value < 0f)
            {
                speed = 0;
            }
            else
            {
                speed = value;
            }
        }
    }
}
