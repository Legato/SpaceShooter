﻿using UnityEngine;
using System.Collections;

public class Wander : BaseBehaviour
{
    public Wander()
    {
        behaviourType = CustomBehaviour.eBehaviourTypes.Wander;
    }

    private float speed;

    public float Speed
    {
        get { return speed; }
        set
        {
            if (value < 0)
            {
                speed = 0;
            }
            else
            {
                speed = value;
            }
        }
    }
}
