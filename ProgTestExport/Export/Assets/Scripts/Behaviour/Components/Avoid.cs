﻿using UnityEngine;
using System.Collections;

public class Avoid : BaseBehaviour {

    public Avoid()
    {
        behaviourType = CustomBehaviour.eBehaviourTypes.Avoid;
    }


    public Transform Target { get; set; }
}
