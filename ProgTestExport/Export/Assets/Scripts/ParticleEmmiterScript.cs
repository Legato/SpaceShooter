﻿using UnityEngine;
using System.Collections;

public class ParticleEmmiterScript : MonoBehaviour
{

    public string systemName;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, 0.05f);
    }

    public ParticleEmitterComponentData GetData()
    {
        ParticleEmitterComponentData data = new ParticleEmitterComponentData();
        data.name = systemName;
        return data;
    }
}
