﻿using UnityEngine;
using System.Collections;

public class LevelChanger : MonoBehaviour
{
    public enum eLevels
    {
        Tutorial = 0,
        Level1,
        Level2,
        Level3,
        Level4,
        End
    }
    public float activationRadius = 5;
    public eLevels level;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, activationRadius);
    }
}
