﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PickUp : NestedSphereCollider
{
    [SerializeField]
    public int pickupType;

    public string Type
    {
        get { return pickupTypes[pickupType]; }
    }

    [SerializeField]
    public static List<string> pickupTypes = new List<string> { "Health", "Speed", "Damage"};

    public new PickupComponentData GetData()
    {
        ColliderComponentData componentData = base.GetData();
        PickupComponentData pickupComponent = new PickupComponentData(componentData);

        if (componentData != null)
        {
            pickupComponent.pickupType = Type;
        }
        

        return pickupComponent;
    }
}
