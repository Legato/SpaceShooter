﻿using UnityEngine;
using System.Collections;

public class ActivateOnDestroy : MonoBehaviour
{
    public Transform objectToActivate;

    public ActivateOnDestroyComponentData GetData()
    {
        if (objectToActivate != null)
        {
            ActivateOnDestroyComponentData data = new ActivateOnDestroyComponentData();
            data.componentToActivate = objectToActivate.name;
            data.componentToActivateId = objectToActivate.gameObject.GetInstanceID();
            return data;
        }
        Debug.Log("ActivateOnDestroy activation object not set.");
        return null;
    }
}
