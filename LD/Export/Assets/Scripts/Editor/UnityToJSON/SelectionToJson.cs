﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

public class SelectionToJson : MonoBehaviour
{

    public static string finalFilePath = SelectionToJsonWindow.saveJSONtoPath + "\\" + SelectionToJsonWindow.nameOfJSONfile;

    [MenuItem("Tools/Convert selection to JSON  %&_e")]

    static void ConverToJSON()
    {
        CreateTextfile();
    }

    public static void CreateTextfile()
    {

        if (System.IO.File.Exists(finalFilePath) == true)
        {
            System.IO.File.Delete(finalFilePath);
        }

        List<GameObject> someObjects = GetSelectionAsGameObjectList();

        SceneData sceneData = new SceneData();

        foreach (GameObject someObject in someObjects)
        {
            ObjectExportType exportType = GetObject(someObject, null);
            if (exportType != null)
            {
                sceneData.Objects.Add(exportType);
            }
        }

        string jsonData = JsonConvert.SerializeObject(sceneData, Formatting.Indented);
        System.IO.File.AppendAllText(finalFilePath, jsonData);

    }

    private static ObjectExportType GetObject(GameObject aGameObject, GameObject aParent)
    {
        if (aGameObject.transform.parent != null && aParent == null || aGameObject.GetComponent<BezierPoint>() != null)
        {
            //this should jump out of function when an object is childed
            return null;
        }

        ObjectExportType template = new ObjectExportType();

        #region Inherent properties

        template.name = aGameObject.name;
        template.tag = aGameObject.tag;
        template.instanceId = aGameObject.GetInstanceID();
        template.isActive = aGameObject.activeSelf;

        //Transform
        Matrix4x4 rawTransform = aGameObject.transform.localToWorldMatrix;

        if (aGameObject.transform.parent != null)
        {
             rawTransform = aGameObject.transform.parent.worldToLocalMatrix * rawTransform;
        }
        template.transform = ExportMatrix4x4.Create(rawTransform);

        //Doesn't seem to do anything, depricated?
        //Vector3 col = template.transform.GetColumn(0);

        //template.transform.SetColumn(0, col);
        //col = template.transform.GetColumn(1);

        //template.transform.SetColumn(1, col);

        //col = template.transform.GetColumn(2);

        //template.transform.SetColumn(2, col);
        //end Transform

    #endregion

    #region Components

    foreach (

    Component component in aGameObject.GetComponents<Component>())
        {
            System.Type componentType = component.GetType();


            //MeshComponent
            if (componentType == typeof(MeshFilter))
            {
                string meshName = GetMeshName((MeshFilter)component);
                MeshComponentData meshData = new MeshComponentData(meshName);
                template.components.Add(meshData);
            }
            //end MeshComponent
            //Lights
            else if (componentType == typeof(Light))
            {
                LightComponentData lightData = new LightComponentData();

                lightData.lightType = GetLightType((Light)component);
                lightData.lightRange = GetLightRange((Light)component);
                lightData.lightColor = ExportVector4.Create(GetLightColor((Light)component));
                lightData.lightIntensity = GetLightIntensity((Light)component);

                template.components.Add(lightData);
            }
            //end Lights
            //Collider
            else if (componentType == typeof(NestedSphereCollider))
            {
                ColliderComponentData componentData = ((NestedSphereCollider) component).GetData();

                template.components.Add(componentData);
            }
            else if (componentType == typeof(PickUp))
            {
                PickupComponentData componentData = ((PickUp)component).GetData();
                template.components.Add(componentData);
            }
            //end Collider
            //EnemyData
            else if (componentType == typeof(EnemyData))
            {
                EnemyComponentData componentData = ((EnemyData) component).GetEnemyData();

                template.components.Add(componentData);
            }
            //end EnemyData
            //Animation
            else if (componentType == typeof(BezierCubicSplineWalker))
            {
                AnimationComponentData componentData = ExportParser.Parse(((BezierCubicSplineWalker) component));
                template.components.Add(componentData);
            }
            //end Animation
            else if (componentType == typeof(RotationScript))
            {
                RotationComponentData componentData = new RotationComponentData(((RotationScript)component).rotationSpeed);
                template.components.Add(componentData);
            }
            else if (componentType == typeof(RandomRotation))
            {
                RandomRotationComponentData componentData = ((RandomRotation) component).GetData();
                template.components.Add(componentData);
            }
            else if (componentType == typeof(LevelChanger))
            {
                LevelChangerComponentData componentData = new LevelChangerComponentData((LevelChanger)component);
                template.components.Add(componentData);
            }
            else if (componentType == typeof(FacePlayer))
            {
                FacePlayerComponentData componentData = new FacePlayerComponentData();
                template.components.Add(componentData);
            }
            else if (componentType == typeof(ActivateOnDestroy))
            {
                ActivateOnDestroyComponentData componentData = ((ActivateOnDestroy) component).GetData();

                if (componentData != null)
                {
                    template.components.Add(componentData);
                }
            }

        }
        #endregion

        #region Parse children
        for (int i = 0; i < aGameObject.transform.childCount; ++i)
        {
            ObjectExportType objectExport = GetObject(aGameObject.transform.GetChild(i).gameObject, aGameObject);
            if (objectExport != null)
            {
                template.components.Add(objectExport);
            }

            
        }

        #endregion

        return template;
    }

    private static List<GameObject> GetSelectionAsGameObjectList()
    {
        List<GameObject> objectsToConvert = new List<GameObject>();
        for (int indexTransform = 0; indexTransform < Selection.transforms.Length; indexTransform++)
        {
            objectsToConvert.Add(PrefabUtility.FindPrefabRoot(Selection.transforms[indexTransform].gameObject));
        }
        return objectsToConvert;
    }

    public static string GetMeshName(MeshFilter aMeshFilter)
    {
        Mesh mesh = aMeshFilter.GetComponent<MeshFilter>().sharedMesh;
        string name = mesh.name;
        string[] subParts = name.Split(' '); // Splits everything after 'space', i.e. splits " Instance" from the name "Sphere Instance"
        return subParts[0] + ".fbx";
    }

#region Lights
    public static string GetLightType(Light aLight)
    {
            string type = aLight.type.ToString();
            return type;
    }

    public static float GetLightRange(Light aLight)
    {
        float range = aLight.range;

        return range;
    }

    public static Vector4 GetLightColor(Light aLight)
    {
        Vector4 color = aLight.color;

        return color;
    }

    public static float GetLightIntensity(Light aLight)
    {
        float intensity = aLight.intensity;

        return intensity;
    }
    #endregion

#region Collider
    public static float GetSphereColliderRadius(SphereCollider aSphereCollider)
    {
        float radius = aSphereCollider.radius;

        return radius;
    }
    public static Vector3 GetSphereColliderOffset(SphereCollider aSphereCollider)
    {
        Vector3 offset = aSphereCollider.center;

        return offset;
        
    }
#endregion

}
