﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(PickUp))]
public class PickupInspector : ColliderInspector
{
    private PickUp pickUp;
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        pickUp = target as PickUp;

        

        EditorGUI.BeginChangeCheck();
        string[] types = PickUp.pickupTypes.ToArray();
        int pickupType = EditorGUILayout.Popup("Pickup type", pickUp.pickupType, types);
        if (EditorGUI.EndChangeCheck() == true)
        {
            Undo.RecordObject(pickUp, "Moved collider center");
            pickUp.pickupType = pickupType;
            EditorUtility.SetDirty(pickUp);
        }

        
    }
	
}
