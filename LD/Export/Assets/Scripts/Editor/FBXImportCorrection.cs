﻿
using UnityEditor;

public class FBXImportCorrection : AssetPostprocessor {

    public void OnPreprocessModel()
    {
        ModelImporter modelImporter = (ModelImporter) assetImporter;
        modelImporter.globalScale = 1 / modelImporter.fileScale;
    }
}
