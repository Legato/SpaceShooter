﻿using UnityEngine;
using System.Collections;
using UnityEditor;


[CustomEditor(typeof(BezierCubicSplineWalker))]
public class AnimationInspector : Editor
{
   
    public override void OnInspectorGUI()
    {
        BezierCubicSplineWalker walker = target as BezierCubicSplineWalker;

        DrawDefaultInspector();

        EditorGUI.BeginChangeCheck();
        float progress = EditorGUILayout.Slider(walker.CurrentTime, 0f, walker.duration);

        if (EditorGUI.EndChangeCheck() == true)
        {
            Undo.RecordObject(walker, "Change progress");
            EditorUtility.SetDirty(walker);
            walker.CurrentTime = progress;
        }
    }
}
