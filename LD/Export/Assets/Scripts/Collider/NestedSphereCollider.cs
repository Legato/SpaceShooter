﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class NestedSphereCollider : MonoBehaviour
{
    [System.Serializable]
    public class InternalCollider
    {
        public InternalCollider(InternalCollider aParent)
        {
            parent = aParent;
        }

        public bool showContent = true;
        public bool showMeshSphere = true;

        [SerializeField]
        public float radius = 1;
        [SerializeField]
        public Vector3 center = new Vector3();

        [SerializeField]
        public InternalCollider parent;
        [SerializeField]
        public List<InternalCollider> children = new List<InternalCollider>();
    }

    public InternalCollider internalCollider = new InternalCollider(null);
    public bool showColliders = true;

    public void Reset()
    {
        
    }

    Color GetColliderDepthColor(uint aDepth)
    {
        Color returnColor;
        switch (aDepth % 6)
        {
            case 0:
                returnColor = Color.green;

                break;
            case 1:
                returnColor = Color.yellow;

                break;
            case 2:
                returnColor = Color.red;

                break;
            case 3:
                returnColor = Color.magenta;

                break;
            case 4:
                returnColor = Color.blue;

                break;
            case 5:
                returnColor = Color.cyan;

                break;

            default:
                returnColor = Color.black;
                
                break;
        }

        return returnColor;
    }

    void OnDrawGizmos()
    {
        if (showColliders == true)
        { 
            DrawCollidersRecursive(0,internalCollider, Vector3.zero);
        }
    }

    void DrawCollidersRecursive(uint aNewDepth, InternalCollider nextCollider, Vector3 parentPosition)
    {
        if (nextCollider.showMeshSphere == true)
        {
            Gizmos.color = GetColliderDepthColor(aNewDepth);
            Gizmos.DrawWireSphere(transform.TransformPoint(parentPosition + nextCollider.center), nextCollider.radius);
        }
        

        foreach (InternalCollider internalColliderChild in nextCollider.children)
        {
            DrawCollidersRecursive(aNewDepth + 1, internalColliderChild, nextCollider.center);
        }
    }

    public ColliderComponentData GetData()
    {
        ColliderComponentData data = new ColliderComponentData();

        data.colliderData.center = ExportVector3.Create(internalCollider.center);
        data.colliderData.radius = internalCollider.radius;

        foreach (InternalCollider internalColliderChild in internalCollider.children)
        {
            data.colliderData.children.Add(GetDataRecursivly(internalColliderChild));
        }

        return data;
    }

    ColliderComponentData.ColliderData GetDataRecursivly(InternalCollider anInternalCollider)
    {
        ColliderComponentData.ColliderData data = new ColliderComponentData.ColliderData();

        data.center = ExportVector3.Create(anInternalCollider.center);
        data.radius = anInternalCollider.radius;

        foreach (InternalCollider child in anInternalCollider.children)
        {
            data.children.Add(GetDataRecursivly(child));
        }

        return data;
    }
}
