﻿using UnityEngine;
using System.Collections;

public class ExportVector4
{
    public static ExportVector4 Create(Vector4 aRawVector)
    {
        ExportVector4 vector4 = new ExportVector4();

        vector4.x = aRawVector.x;
        vector4.y = aRawVector.y;
        vector4.z = aRawVector.z;
        vector4.w = aRawVector.w;

        return vector4;
    }
    public float x, y, z, w;
}
