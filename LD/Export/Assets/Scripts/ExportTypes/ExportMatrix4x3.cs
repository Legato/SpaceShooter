﻿using UnityEngine;

public class ExportMatrix4x4
{
    public static ExportMatrix4x4 Create(Matrix4x4 rawTransform)
    {
        ExportMatrix4x4 matrix = new ExportMatrix4x4();

        matrix.m00 = rawTransform.m00;
        matrix.m01 = rawTransform.m01;
        matrix.m02 = rawTransform.m02;
        matrix.m03 = rawTransform.m03;

        matrix.m10 = rawTransform.m10;
        matrix.m11 = rawTransform.m11;
        matrix.m12 = rawTransform.m12;
        matrix.m13 = rawTransform.m13;

        matrix.m20 = rawTransform.m20;
        matrix.m21 = rawTransform.m21;
        matrix.m22 = rawTransform.m22;
        matrix.m23 = rawTransform.m23;

        matrix.m30 = rawTransform.m30;
        matrix.m31 = rawTransform.m31;
        matrix.m32 = rawTransform.m32;
        matrix.m33 = rawTransform.m33;

        return matrix;
    }

    public float m00;
    public float m01;
    public float m02;
    public float m03;

    public float m10;
    public float m11;
    public float m12;
    public float m13;

    public float m20;
    public float m21;
    public float m22;
    public float m23;

    public float m30;
    public float m31;
    public float m32;
    public float m33;
}
