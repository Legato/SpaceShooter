﻿using UnityEngine;
using System.Collections;

public class LevelChangerComponentData : BaseComponentData
{
    public LevelChangerComponentData()
    {
        componentType = "levelChanger";
    }

    public LevelChangerComponentData(LevelChanger aLevelChanger) : this()
    {
        radius = aLevelChanger.activationRadius;
        levelIndex = (int)aLevelChanger.level;
    }

    public float radius;
    public int levelIndex = -1;
}
