﻿using UnityEngine;
using System.Collections;

public class PickupComponentData : ColliderComponentData
{
    public PickupComponentData()
    {
        componentType = "pickup";
    }

    public PickupComponentData(ColliderComponentData someColliderComponentData) : this()
    {
        colliderData = someColliderComponentData.colliderData;
    }

    public string pickupType;
}
