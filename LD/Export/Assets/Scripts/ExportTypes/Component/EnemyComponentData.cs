﻿using UnityEngine;
using System.Collections;

public class EnemyComponentData : BaseComponentData {

    public EnemyComponentData()
    {
        componentType = "enemy";
    }

    public short maxHp;
    public float speed;
    public float fireRate;
    public short damage;
    public float range;
    public ExportVector3 shootingOffset;
}
