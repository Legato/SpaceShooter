﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

public class SelectionToJson : MonoBehaviour
{

    public static string finalFilePath = SelectionToJsonWindow.saveJSONtoPath + "\\" + SelectionToJsonWindow.nameOfJSONfile;

    [MenuItem("Tools/Convert selection to JSON  %&_e")]

    static void ConverToJSON()
    {
        CreateTextfile();
    }

    public static void CreateTextfile()
    {

        if (System.IO.File.Exists(finalFilePath) == true)
        {
            System.IO.File.Delete(finalFilePath);
        }

        List<GameObject> someObjects = GetSelectionAsGameObjectList();
        System.IO.File.AppendAllText(finalFilePath, "{" + "\r\n" + "\"Objects\":\r\n[" + "\r\n\t" + "\r\n\t");

        for (int indexGameObject = 0; indexGameObject < someObjects.Count; indexGameObject++)
        {

            if (indexGameObject == someObjects.IndexOf(someObjects.Last()))
            {
                CreateJsonFile(someObjects[indexGameObject], true);
            }
            else
            {
                CreateJsonFile(someObjects[indexGameObject], false);
            }
        }

        System.IO.File.AppendAllText(finalFilePath, "\r\n\t" + "\r\n" + "]" + "\r\n" + "}");

    }

    public static void CreateJsonFile(GameObject aGameObject, bool aIsLastObject)
    {

        ConvertGameObjectToJSON(aGameObject);

        if (!aIsLastObject)
        {
            System.IO.File.AppendAllText(finalFilePath, "," + "\r\n");
        }

    }

    public class ExportTemplate
    {

        public string name;
        public int ID;
        public string mesh;
        public string tag;
        public Matrix4x4 transform;
        public Vector3 rotation;
        public string parent;
        public string lightType;
        public float lightRange;
        public Vector4 lightColor;
        public float lightIntensity;
        public AnimationInfo animationInfo;
        public float sphereColliderRadius;

    }

    public static void ConvertGameObjectToJSON(GameObject aGameObject)
    {
        Debug.Log(aGameObject.transform.localRotation.eulerAngles);
        ExportTemplate template = new ExportTemplate();

        template.name = aGameObject.name;
        template.ID = aGameObject.GetInstanceID();
        template.mesh = GetMeshName(aGameObject);
        template.tag = aGameObject.tag;
        template.transform = aGameObject.transform.localToWorldMatrix;

        if (template.mesh != null)
        {
            Vector3 col = template.transform.GetColumn(0);

            template.transform.SetColumn(0, col);
            col = template.transform.GetColumn(1);

            template.transform.SetColumn(1, col);

            col = template.transform.GetColumn(2);

            template.transform.SetColumn(2, col);
        }

        if (aGameObject.transform.parent != null)
        {
            template.parent = aGameObject.transform.parent.name;
        }

        template.rotation = aGameObject.transform.localRotation.eulerAngles;
        Debug.Log(template.rotation);


        template.lightType = GetLightType(aGameObject);
        template.lightRange = GetLightRange(aGameObject);
        template.lightColor = GetLightColor(aGameObject);
        template.lightIntensity = GetLightIntensity(aGameObject);
        BezierCubicSplineWalker walker = aGameObject.GetComponent<BezierCubicSplineWalker>();

        template.sphereColliderRadius = GetSphereColliderRadius(aGameObject);

        if (walker != null)
        {
            template.animationInfo = walker.GetInfo();
        }


        System.IO.File.AppendAllText(finalFilePath, JsonUtility.ToJson(template, true));

        for (int i = 0; i < aGameObject.transform.childCount; ++i)
        {
            System.IO.File.AppendAllText(finalFilePath, "," + "\r\n");
            ConvertGameObjectToJSON(aGameObject.transform.GetChild(i).gameObject);
        }

    }

    private static List<GameObject> GetSelectionAsGameObjectList()
    {
        List<GameObject> objectsToConvert = new List<GameObject>();
        for (int indexTransform = 0; indexTransform < Selection.transforms.Length; indexTransform++)
        {
            objectsToConvert.Add(PrefabUtility.FindPrefabRoot(Selection.transforms[indexTransform].gameObject));
        }
        return objectsToConvert;
    }

    public static string GetMeshName(GameObject aMeshGameObject)
    {
        if (aMeshGameObject.GetComponent<MeshFilter>() != null)
        {
            Mesh mesh = aMeshGameObject.GetComponent<MeshFilter>().sharedMesh;
            string name = mesh.name;
            string[] subParts = name.Split(' '); // Splits everything after 'space', i.e. splits " Instance" from the name "Sphere Instance"
            return subParts[0] + ".fbx";
        }
        return "";
    }

    public static string GetLightType(GameObject aLightObject)
    {
        if (aLightObject.GetComponent<Light>() != null)
        {
            Light light = aLightObject.GetComponent<Light>();
            string type = light.type.ToString();
            return type;
        }
        return "";
    }

    public static float GetLightRange(GameObject aLightObject)
    {
        if (aLightObject.GetComponent<Light>() != null)
        {
            Light light = aLightObject.GetComponent<Light>();
            float range = light.range;

            return range;
        }
        return 0;
    }

    public static Vector4 GetLightColor(GameObject aLightObject)
    {
        if (aLightObject.GetComponent<Light>() != null)
        {
            Light light = aLightObject.GetComponent<Light>();
            Vector4 color = light.color;

            return color;
        }
        return Vector4.zero;
    }

    public static float GetLightIntensity(GameObject aLightObject)
    {
        if (aLightObject.GetComponent<Light>() != null)
        {
            Light light = aLightObject.GetComponent<Light>();
            float intensity = light.intensity;

            return intensity;
        }
        return 0;
    }

    public static float GetSphereColliderRadius(GameObject aGameObject)
    {
        if (aGameObject.GetComponent<SphereCollider>() != null)
        {
            SphereCollider collider = aGameObject.GetComponent<SphereCollider>();
            float radius = collider.radius;

            return radius;
        }
        return 0;
    }
}
