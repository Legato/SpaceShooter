﻿using UnityEngine;
using UnityEditor;

public class SelectionToJsonWindow : EditorWindow
{

    public static string saveJSONtoPath = @"C:\Users\stemgl1\Desktop\";
    public static string nameOfJSONfile = "GameObjects.json";

    [MenuItem("Tools/JSON Exporter Settings")]
    static void OpenWindow()
    {

        SelectionToJsonWindow window = (SelectionToJsonWindow)EditorWindow.GetWindow<SelectionToJsonWindow>(typeof(SelectionToJsonWindow));
        window.Show();

    }

    void OnGUI()
    {

        EditorGUILayout.LabelField("JSON Exporter Settings" + "\r\n", EditorStyles.boldLabel);

        saveJSONtoPath = EditorGUILayout.TextField("File Path Save", saveJSONtoPath);
        EditorPrefs.SetString("saveJSONtoPath", saveJSONtoPath);

        nameOfJSONfile = EditorGUILayout.TextField("JSON textfile name", nameOfJSONfile);
        EditorPrefs.SetString("nameOfJSONfile", nameOfJSONfile);

        if (GUILayout.Button("Exit"))
        {
            SelectionToJsonWindow window = (SelectionToJsonWindow)EditorWindow.GetWindow<SelectionToJsonWindow>(typeof(SelectionToJsonWindow));
            window.Close();
        }

    }

    void OnEnable()
    {
        if (EditorPrefs.HasKey("saveJSONtoPath"))
        {
            saveJSONtoPath = EditorPrefs.GetString("saveJSONtoPath", saveJSONtoPath);
        }

        if (EditorPrefs.HasKey("nameOfJSONfile"))
        {
            nameOfJSONfile = EditorPrefs.GetString("nameOfJSONfile", nameOfJSONfile);
        }
    }

}