﻿using UnityEditor;
using UnityEngine;
[InitializeOnLoad]
class MyClass: Editor
{
    static MyClass()
    {
        EditorApplication.update += Update;
    }

    static void Update()
    {
        if (AutoSnapToggle.IsActivated)
        {
            Snap();
        }
    }

    static void Snap()
    {
        foreach (var transform in Selection.transforms)
        {
            Vector3 v = transform.position;
            v.x = RoundX(v.x);
            v.y = RoundVertical(v.y);
            v.z = RoundZ(v.z);
            transform.position = v;
        }
    }

    static float RoundX(float input)
    {
        return EditorPrefs.GetFloat("MoveSnapX") * Mathf.Round((input / EditorPrefs.GetFloat("MoveSnapX")));
    }
    static float RoundZ(float input)
    {
        return EditorPrefs.GetFloat("MoveSnapY") * Mathf.Round((input / EditorPrefs.GetFloat("MoveSnapY")));
    }
    static float RoundVertical(float input)
    {
        return EditorPrefs.GetFloat("MoveSnapZ") * Mathf.Round((input / EditorPrefs.GetFloat("MoveSnapZ")));
    }
}