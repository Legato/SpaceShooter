﻿using UnityEngine;
using UnityEditor;


public class AutoSnapToggle: Editor {

    public static bool IsActivated = false;

    [MenuItem("Edit/Toggle Grid Auto-Snapping %&_G")]
    static void ToggleAutoSnap()
    {
        IsActivated = !IsActivated;

        if (IsActivated == true)
        {
            Debug.Log("*** AUTO-SNAP TOOL IS NOW ___ACTIVATED___***");
        }
        else
        {
            Debug.Log("*** AUTO-SNAP TOOL IS NOW ___DEACTIVATED___***");
        }
       
    }
}
