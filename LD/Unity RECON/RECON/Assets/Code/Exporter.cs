﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class ExportAllTxt : EditorWindow
{
    static TextWriter myTextWriter;

    [MenuItem("File/Export/Export All Triggers #%_e")]

    static void BeginExportTrigger()
    {
        string path = "trigger.txt";
        myTextWriter = new StreamWriter(path);

        Trigger_Sphere[] trigger = GameObject.FindObjectsOfType<Trigger_Sphere>();
        foreach (Trigger_Sphere t in trigger)
        {
            ExportTriggerTransform(t);
        }

        myTextWriter.Close();
        EndExport();
    }

    [MenuItem("File/Export/Export All #&_e")]

    static void BeginExportAll()
    {
        string path = "all.txt";
        myTextWriter = new StreamWriter(path);

        Trigger_Sphere[] triggers = GameObject.FindObjectsOfType<Trigger_Sphere>();
        myTextWriter.WriteLine("{\"TRIGGERS\":[");
        foreach (Trigger_Sphere t in triggers)
        {
            myTextWriter.WriteLine("{");
            ExportTriggerID(t);
            ExportTriggerTransform(t);
            ExportTriggerLevelClear(t);
            myTextWriter.WriteLine("},");

        }
        myTextWriter.WriteLine("],");

        GameObject[] gameObjects = GameObject.FindObjectsOfType<GameObject>();
        myTextWriter.WriteLine("{\"GAMEOBJECTS\":[");
        foreach (GameObject go in gameObjects)
        {
            if (!IsOnBlackList(go))
            {
                myTextWriter.WriteLine("{");
                ExportGoID(go);
                ExportMesh(go);
                ExportTransform(go);
                myTextWriter.WriteLine("},");
            }           
        }
        myTextWriter.WriteLine("],");

        Light[] lights = FindObjectsOfType<Light>();
        myTextWriter.WriteLine("\"LIGHTS\":[");
        foreach (Light t in lights)
        {
            myTextWriter.WriteLine("{");
            ExportLightID(t);
            ExportLightTransform(t);
            ExportLightType(t);
            ExportLightIntensity(t);
            ExportLightColor(t);
            myTextWriter.WriteLine("},");
        }
        myTextWriter.WriteLine("]}");

        myTextWriter.Close();
        EndExport();
    }

    private static string ExportTriggerTransform(Trigger_Sphere aTrigger)
    {
        Matrix4x4 transform = aTrigger.gameObject.GetComponent<Transform>().localToWorldMatrix;
        string returnString = "\"transformation\":[" + (transform.m00).ToString() + ", " + (transform.m01).ToString() + ", " + (transform.m02).ToString() + ", 0" + ", " +
                              (transform.m10).ToString() + ", " + (transform.m11).ToString() + ", " + (transform.m12).ToString() + ", 0" + ", " +
                              (transform.m20).ToString() + ", " + (transform.m21).ToString() + ", " + (transform.m22).ToString() + ", 0" + ", " +
                              (transform.m30).ToString() + ", " + (transform.m31).ToString() + ", " + (transform.m32).ToString() + ", 1" + "]";
        myTextWriter.WriteLine(returnString);


        return returnString;
    }

    private static void ExportTriggerLevelClear(Trigger_Sphere aTrigger)
    {
        bool cleared = aTrigger.myLevelCleared;
        string returnString = "\"levelClearedTrigger\":" + cleared.ToString() + ",";
        myTextWriter.WriteLine(returnString);

    }

    private static void ExportMesh(GameObject aMeshGameObject)
    {
        if (aMeshGameObject.GetComponent<MeshFilter>())
        {
            // WRITE MESH
            string name = GetMeshName(aMeshGameObject);
            string meshLine = "\"Mesh\":\"" + name + ".fbx\",";

            myTextWriter.WriteLine(meshLine);
        }
    }

    protected static void DoWrite(string aString)
    {
        myTextWriter.WriteLine(aString);
    }

    static void EndExport()
    {
        Debug.Log("Export complete!");
    }

    public static string GetPath(Object anObject)
    {
        // Gets path of a prefab.
        string path = AssetDatabase.GetAssetPath(anObject);
        return path;
    }

    public static string ExportGoID(GameObject aGameObject)
    {
        string myID = aGameObject.GetInstanceID().ToString();
        string returnString = "\"ID\":" + myID + ",";
        myTextWriter.WriteLine(returnString);

        return returnString;
    }

    public static string ExportLightID(Light aLightObject)
    {
        string myID = aLightObject.GetInstanceID().ToString();
        string returnString = "\"ID\":" + myID + ",";
        myTextWriter.WriteLine(returnString);

        return returnString;
    }
    public static string ExportTriggerID(Trigger_Sphere aTrigger)
    {
        string myID = aTrigger.GetInstanceID().ToString();
        string returnString = "\"ID\":" + myID + ",";
        myTextWriter.WriteLine(returnString);

        return returnString;
    }

    public static string GetMeshName(GameObject aMeshGameObject)
    {
        Mesh mesh = aMeshGameObject.GetComponent<MeshFilter>().sharedMesh;
        string name = mesh.name;
        string[] subParts = name.Split(' '); // Splits everything after 'space', i.e. splits " Instance" from the name "Sphere Instance"
        return subParts[0];
    }

    public static string ExportTransform(GameObject aGameObject)
    {
        Matrix4x4 transform = aGameObject.GetComponent<Transform>().localToWorldMatrix;
        string returnString = "\"transformation\":[" + (transform.m00).ToString() + ", " + (transform.m01).ToString() + ", " + (transform.m02).ToString() + ", 0"  + ", " +
                              (transform.m10).ToString() + ", " + (transform.m11).ToString() + ", " + (transform.m12).ToString() + ", 0"  + ", " +
                              (transform.m20).ToString() + ", " + (transform.m21).ToString() + ", " + (transform.m22).ToString() + ", 0"  + ", " +
                              (transform.m30).ToString() + ", " + (transform.m31).ToString() + ", " + (transform.m32).ToString() + ", 1"  + "]";
        myTextWriter.WriteLine(returnString);


        return returnString;
    }

    public static string ExportLightTransform(Light aLightObject)
    {
        Matrix4x4 transform = aLightObject.GetComponent<Transform>().localToWorldMatrix;
        string returnString = "\"transformation\":[" + (transform.m00).ToString() + ", " + (transform.m01).ToString() + ", " + (transform.m02).ToString() + ", 0"  + ", " +
                              (transform.m10).ToString() + ", " + (transform.m11).ToString() + ", " + (transform.m12).ToString() + ", 0"  + ", " +
                              (transform.m20).ToString() + ", " + (transform.m21).ToString() + ", " + (transform.m22).ToString() + ", 0"  + ", " +
                              (transform.m30).ToString() + ", " + (transform.m31).ToString() + ", " + (transform.m32).ToString() + ", 1"  + "]";
        myTextWriter.WriteLine(returnString);


        return returnString;
    }

    public static string ExportLightType(Light aLightObject)
    {
        Light light = aLightObject as Light;
        string returnString = "";
        if (light != null)
        {
            string type = light.type.ToString();

            myTextWriter.WriteLine("\"type\":\"" + type + "\",");
        }

        return returnString;
    }

    public static string ExportLightColor(Light aLightObject)
    {

        Light light = aLightObject as Light;
        string returnString = "";
        if (light != null)
        {
            string color = light.color.ToString();
            color = "[" + light.color.r + ", " + light.color.g + ", " + light.color.b + ", " + light.color.a + "]";

            myTextWriter.WriteLine("\"color\":" + color + ",");
        }

        return returnString;
    }
    public static string ExportLightIntensity(Light aLightObject)
    {


        Light light = aLightObject as Light;
        string returnString = "";
        if (light != null)
        {
            string intensity = light.intensity.ToString();            

            myTextWriter.WriteLine("\"intensity\":" + intensity + ",");
        }

        return returnString;
    }

    public static bool IsOnBlackList(GameObject aGameObject)
    {
        bool isOnBlackList = false;
        if (aGameObject.GetComponent<Camera>())
        {
            isOnBlackList = true;
        }
        if (aGameObject.GetComponent<Terrain>())
        {
            isOnBlackList = true;
        }
        if (aGameObject.GetComponent<Light>())
        {
            isOnBlackList = true;
        }
        if (aGameObject.GetComponent<Trigger_Sphere>())
        {
            isOnBlackList = true;
        }
        return isOnBlackList;
    }


}
