﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(BezierCubicCurve))]
public class BezierCubicViewer : Editor {

    private BezierCubicCurve curve;
    private Transform handleTransform;
    private Quaternion handleRotation;

    private void OnSceneGUI()
    {
        curve = target as BezierCubicCurve;
        handleTransform = curve.transform;
        handleRotation = Tools.pivotRotation == PivotRotation.Local ? handleTransform.rotation : Quaternion.identity;

        Vector3 p0 = ShowPoint(0);
        Vector3 p1 = ShowPoint(1);
        Vector3 p2 = ShowPoint(2);
        Vector3 p3 = ShowPoint(3);

        Handles.color = Color.cyan;
        Handles.DrawLine(p0, p1);
        Handles.color = Color.red;
        Handles.DrawLine(p2, p3);


        ShowDirections();
        ShowCurve();
    }

    private void ShowCurve()
    {
        Handles.color = Color.blue;

        int lineSteps = curve.numberOfSteps;
        Vector3 lineStart = curve.GetPoint(0f);
        for (int i = 1; i <= lineSteps; ++i)
        {
            Vector3 lineEnd = curve.GetPoint(i / (float)lineSteps);

            
            Handles.DrawLine(lineStart, lineEnd);
            lineStart = lineEnd;
        }

        //Handles.DrawBezier(p0, p3, p1, p2, Color.blue, null, 2);
    }

    private void ShowDirections()
    {
        if (curve.showDirections == true)
        {
            Handles.color = Color.green;
            int lineSteps = curve.numberOfSteps;

            for (int i = 1; i < lineSteps; ++i)
            {
                Vector3 lineEnd = curve.GetPoint(i/(float) lineSteps);

                
                Handles.DrawLine(lineEnd, lineEnd + curve.GetDirection(i/(float) lineSteps));

            }
        }
    }

    private Vector3 ShowPoint(int index)
    {
        Vector3 point = handleTransform.TransformPoint(curve.points[index]);

        EditorGUI.BeginChangeCheck();
        point = Handles.DoPositionHandle(point, handleRotation);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(curve, "Move Point");
            EditorUtility.SetDirty(curve);
            curve.points[index] = handleTransform.InverseTransformPoint(point);
        }

        return point;
    }
}
