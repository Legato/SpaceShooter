﻿using UnityEngine;
using System.Collections;
using System.Runtime.Remoting.Messaging;
using UnityEditor;


[CustomEditor(typeof(BezierCubicSpline))]
[ExecuteInEditMode]
public class BezierCubicSplineInspector : Editor {

    private BezierCubicSpline spline;
    private Transform handleTransform;
    private Quaternion handleRotation;

    private const float handleSize = 0.03f;
    private const float pickSize = 0.05f;

    private int selectedIndex = -1;
    


    private void OnSceneGUI()
    {
        

        spline = target as BezierCubicSpline;
        Matrix4x4 parentTransform = spline.GetComponentInParent<Transform>().localToWorldMatrix;
        float rotation = spline.GetComponentInParent<Transform>().rotation.eulerAngles.x;
        selectedIndex = spline.selectedIndex;

        handleTransform = spline.transform;
        handleRotation = Tools.pivotRotation == PivotRotation.Local ? handleTransform.rotation : Quaternion.identity;


        if (spline.showCurve == true)
        {
            Vector3 p0 = ShowPoint(0);

            for (int i = 1; i < spline.ControlPointCount; i += 3)
            {
                Vector3 p1 = ShowPoint(i);
                Vector3 p2 = ShowPoint(i + 1);
                Vector3 p3 = ShowPoint(i + 2);

                Handles.color = Color.cyan;
                Handles.DrawLine(p0, p1);
                Handles.color = Color.red;
                Handles.DrawLine(p2, p3);


                ShowDirections(p0, p1, p2, p3);
                ShowCurve(p0, p1, p2, p3);

                p0 = p3;
            }
        }

        spline.selectedIndex = selectedIndex;
    }

    public override void OnInspectorGUI()
    {
        spline = target as BezierCubicSpline;

        selectedIndex = spline.selectedIndex;

        if (GUILayout.Button("Select First Point") == true)
        {
            selectedIndex = 0;
        }
        if (GUILayout.Button("Select Last Point") == true)
        {
            selectedIndex = spline.ControlPointCount - 1;
        }


        DrawSelectedPointInspector();
        

        if (GUILayout.Button("Add Curve") == true)
        {
            Undo.RecordObject(spline, "Add curve");
            spline.AddCurve();
            EditorUtility.SetDirty(spline);
        }

        EditorGUI.BeginChangeCheck();
        int subdivisions =
            EditorGUILayout.IntField("Segments per curve spline", spline.numberOfSteps);
        if (EditorGUI.EndChangeCheck() == true)
        {
            Undo.RecordObject(spline, "Change Number of Segments");
            spline.numberOfSteps = subdivisions;
            EditorUtility.SetDirty(spline);
        }

        EditorGUI.BeginChangeCheck();
        bool showCurve =
            EditorGUILayout.Toggle("Show Curve", spline.showCurve);
        if (EditorGUI.EndChangeCheck() == true)
        {
            Undo.RecordObject(spline, "Change Show Curve Tangents");
            spline.showCurve = showCurve;

            if (spline.showCurve == false)
            {
                selectedIndex = -1;
            }

            EditorUtility.SetDirty(spline);
        }

        EditorGUI.BeginChangeCheck();
        bool showDirection =
            EditorGUILayout.Toggle("Show Curve Tangents", spline.showDirections);
        if (EditorGUI.EndChangeCheck() == true)
        {
            Undo.RecordObject(spline, "Change Show Curve Tangents");
            spline.showDirections = showDirection;
            EditorUtility.SetDirty(spline);
        }

        //TODO:This does not work, fix
        EditorGUI.BeginChangeCheck();
        bool loop = EditorGUILayout.Toggle("Loop", spline.Loop);
        if (EditorGUI.EndChangeCheck() == true)
        {
            Undo.RecordObject(spline, "Toggle Loop");
            spline.Loop = loop;
            EditorUtility.SetDirty(spline);
        }

        EditorGUI.BeginChangeCheck();
        bool displayAxees = EditorGUILayout.Toggle("Show axees", spline.showAxees);
        if (EditorGUI.EndChangeCheck() == true)
        {
            spline.showAxees = displayAxees;
        }

        spline.selectedIndex = selectedIndex;
    }

    private void DrawSelectedPointInspector()
    {
        if (selectedIndex >= 0 && selectedIndex < spline.ControlPointCount)
        {
            GUIStyle style = new GUIStyle();
            style.margin.bottom = 30;
            style.margin.right = 10;
            style.margin.left = 10;
            style.margin.bottom = 10;

            EditorGUILayout.BeginVertical(style);
            GUILayout.Label("Selected Point");
            EditorGUI.BeginChangeCheck();
            Vector3 point = EditorGUILayout.Vector3Field("Position", spline.GetControlPoint(selectedIndex));
            if (EditorGUI.EndChangeCheck() == true)
            {
                Undo.RecordObject(spline, "Move Point");
                EditorUtility.SetDirty(spline);
                spline.SetControlPoint(selectedIndex, point);
            }

            EditorGUI.BeginChangeCheck();
            Quaternion transform = spline.GetControlRotation(selectedIndex);

            Vector3 rotation = EditorGUILayout.Vector3Field("Rotation", transform.eulerAngles);
            if (EditorGUI.EndChangeCheck() == true)
            {
                Undo.RecordObject(spline, "Change rotation");
                EditorUtility.SetDirty(spline);
                Quaternion newRotation = new Quaternion();
                newRotation.eulerAngles = rotation;
                spline.SetControlRotation(selectedIndex, newRotation);

            }

            EditorGUI.BeginChangeCheck();
            BezierControlPointMode mode =
                (BezierControlPointMode)EditorGUILayout.EnumPopup("Mode", spline.GetControlPointMode(selectedIndex));
            if (EditorGUI.EndChangeCheck() == true)
            {
                Undo.RecordObject(spline, "Change Point Mode");
                spline.SetControlPointMode(selectedIndex, mode);
                EditorUtility.SetDirty(spline);
            }

            if (GUILayout.Button("Deselect") == true)
            {
                selectedIndex = -1;
            }
            EditorGUILayout.EndVertical();
        }

        
    }

    private void ShowCurve(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        
            

        int lineSteps = spline.numberOfSteps*spline.CurveCount;

        Matrix4x4 startTrans = spline.GetPoint(0f);

        Handles.color = Color.blue;

        for (int i = 1; i <= lineSteps; ++i)
        {
            Matrix4x4 transEnd = spline.GetPoint(i/(float) lineSteps);

            
            Handles.color = Color.blue;
            Vector3 start = startTrans.GetColumn(3);
            Vector3 end = transEnd.GetColumn(3);
            Handles.DrawLine(start, end);


            if (spline.showAxees == true)
            {
                Handles.color = Color.red;
                Vector3 dir = startTrans.GetColumn(0).normalized;
                Handles.DrawLine(start, start + dir);

                Handles.color = Color.green;
                dir = startTrans.GetColumn(1).normalized;
                Handles.DrawLine(start, start + dir);

                Handles.color = Color.cyan;
                dir = startTrans.GetColumn(2).normalized;
                Handles.DrawLine(start, start + dir);
                
            }
            
            startTrans = transEnd;
        }
        
    }

    private void ShowDirections(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        if (spline.showDirections == true)
        {
            Handles.color = Color.green;
            int lineSteps = spline.numberOfSteps * spline.CurveCount;

            for (int i = 1; i < lineSteps; ++i)
            {
                Vector3 lineEnd = spline.GetPoint(i / (float)lineSteps).GetColumn(3);


                Handles.DrawLine(lineEnd, lineEnd + spline.GetDirection(i / (float)lineSteps));

            }
        }
    }
    
    private static Color[] modeColors =
    {
        Color.green,
        Color.yellow,
        Color.magenta
    };

    private Vector3 ShowPoint(int index)
    {
        Vector3 point = handleTransform.TransformPoint(spline.GetControlPoint(index));

        float size = HandleUtility.GetHandleSize(point);

        if (index == 0 || index == spline.ControlPointCount - 1)
        {
            size *= 2f;
        }

        Handles.color = modeColors[(int)spline.GetControlPointMode(index)];

        if (Handles.Button(point, handleRotation, size * handleSize, size * pickSize, Handles.DotCap) == true)
        {
            selectedIndex = index;
            Repaint();
        }

        if (selectedIndex == index)
        {
            EditorGUI.BeginChangeCheck();
            point = Handles.DoPositionHandle(point, handleRotation);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(spline, "Move Point");
                EditorUtility.SetDirty(spline);
                spline.SetControlPoint(index, handleTransform.InverseTransformPoint(point));
            }
        }
        
        return point;
    }
}
