﻿using UnityEngine;

using System.Collections;
using UnityEditor;

[CustomEditor(typeof(BezierQuadraticCurve))]
public class BezierQuadraticViewer : Editor
{
    private BezierQuadraticCurve curve;
    private Transform handleTransform;
    private Quaternion handleRotation;

    

    private void OnSceneGUI()
    {
        curve = target as BezierQuadraticCurve;
        handleTransform = curve.transform;
        handleRotation = Tools.pivotRotation == PivotRotation.Local ? handleTransform.rotation : Quaternion.identity;

        Vector3 p0 = ShowPoint(0);
        Vector3 p1 = ShowPoint(1);
        Vector3 p2 = ShowPoint(2);

        Handles.color = Color.white;
        Handles.DrawLine(p0, p1);
        Handles.DrawLine(p1, p2);

        
        Vector3 lineStart = curve.GetPoint(0f);

        float lineSteps = curve.numberOfSteps;
        for (int i = 0; i <= lineSteps; ++i)
        {
            Handles.color = Color.blue;

            Vector3 lineEnd = curve.GetPoint(i/(float) lineSteps);
            Handles.DrawLine(lineStart, lineEnd);

            if (curve.showDirections == true)
            {
                Handles.color = Color.green;
                Handles.DrawLine(lineEnd, lineEnd + curve.GetDirection(i / (float)lineSteps));
            }
            

            lineStart = lineEnd;
        }
    }

    private void ShowCurve()
    {
        Vector3 lineStart = curve.GetPoint(0f);

        float lineSteps = curve.numberOfSteps;
        for (int i = 1; i <= lineSteps; ++i)
        {
            Handles.color = Color.blue;

            Vector3 lineEnd = curve.GetPoint(i / (float)lineSteps);

            Handles.DrawLine(lineStart, lineEnd);


            lineStart = lineEnd;
        }
    }

    private void ShowDirections()
    {
        if (curve.showDirections == true)
        {
            Handles.color = Color.green;
            float lineSteps = curve.numberOfSteps;
            for (int i = 1; i < lineSteps; ++i)
            {
                Vector3 lineEnd = curve.GetPoint(i/(float) lineSteps);
                
                Handles.DrawLine(lineEnd, lineEnd + curve.GetDirection(i/(float) lineSteps));
            }
        }
    }


    private Vector3 ShowPoint(int index)
    {
        Vector3 point = handleTransform.TransformPoint(curve.points[index]);

        EditorGUI.BeginChangeCheck();
        point = Handles.DoPositionHandle(point, handleRotation);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(curve, "Move Point");
            EditorUtility.SetDirty(curve);
            curve.points[index] = handleTransform.InverseTransformPoint(point);
        }

        return point;
    }
}
