﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;

[System.Serializable]
public class AnimationInfo
{
    public float duration;
    public BezierCubicSplineWalker.SpliWalkerMode mode;
    public bool lookForward;
    public BezierSplineInfo spline;
};

[System.Serializable]
public class BezierCubicSplineWalker : MonoBehaviour, ISerializable
{
    public AnimationInfo GetInfo()
    {
        AnimationInfo info = new AnimationInfo();
        info.duration = duration;
        info.mode = mode;
        info.lookForward = lookForward;
        info.spline = spline.GetSplineInfo();

        return info;
    }

    public enum SpliWalkerMode
    {
        Once,
        Loop,
        PingPong
    }

    public BezierCurve spline;
    public float duration;
    public SpliWalkerMode mode;
    private bool goingForward;
    public bool lookForward = true;

    private float progress;

    public float Progress
    {
        get { return progress; }
        set { progress = Mathf.Clamp01(value); UpdateProgress(); }
    }

    public float CurrentTime
    {
        get { return duration * progress; }
        set { Progress = value / duration; }
    }

    private void Update()
    {
        if (goingForward == true)
        {
            progress += Time.deltaTime / duration;
        }
        else
        {
            progress -= Time.deltaTime / duration;

        }

        UpdateProgress();
    }

    private void UpdateProgress()
    {
        if (spline != null)
        {
            if (goingForward == true)
            {
                if (progress > 1f)
                {
                    if (mode == SpliWalkerMode.Once)
                    {
                        progress = 1f;
                    }
                    else if (mode == SpliWalkerMode.Loop)
                    {
                        progress -= 1f;
                    }
                    else
                    {
                        progress = 2f - progress;
                        goingForward = false;
                    }
                }
            }
            else
            {
                if (progress < 0)
                {
                    progress = -progress;
                    goingForward = true;
                }
            }

            Vector3 position = spline.GetPointAt(progress);
            transform.localPosition = position;

            if (lookForward == true)
            {
                if (goingForward == true)
                {
                    transform.LookAt(position + spline.GetDirection(progress));
                }
                else
                {
                    transform.LookAt(position - spline.GetDirection(progress));
                }

            }
        }
    }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.AddValue("duration", duration);
        info.AddValue("mode", mode);
        info.AddValue("lookForward", lookForward);
        info.AddValue("spline", spline);
    }
}
