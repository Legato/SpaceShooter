﻿using UnityEngine;
using System.Collections;

public static class Bezier
{
    public static Vector3 GetPoint(Vector3 p0, Vector3 p1, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;

        return
            oneMinusT * p0 + t * p1;
    }

    public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;

        return
            oneMinusT * oneMinusT * p0 +
            2 * oneMinusT * t * p1 +
            t * t * p2;
    }

    public static Vector3 GetDerivate(Vector3 p0, Vector3 p1, Vector3 p2, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;

        return 2 * oneMinusT * (p1 - p0) + 2 * t * (p2 - p1);
    }

    public static Vector3 GetSecondDerivate(Vector3 p0, Vector3 p1, Vector3 p2, float t)
    {
        return 2 * (p2 - 2 * p1 + p0);
    }

    public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;

        return
            oneMinusT * oneMinusT * oneMinusT * p0 +
            3f * oneMinusT * oneMinusT * t * p1 +
            3f * oneMinusT * t * t * p2 +
            t * t * t * p3;
    }

    public static Vector3 GetDerivate(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;

        return 
            3*oneMinusT*oneMinusT*(p1 - p0) + 
            6*oneMinusT*t*(p2 - p1) + 
            3*t*t*(p3 - p2);
    }

    public static Vector3 GetSecondDerivate(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;

        return
            6*oneMinusT*(p2 - 2*p1 + p1) + 
            6*t*(p3 - 2*p2 + p1);
    }
}