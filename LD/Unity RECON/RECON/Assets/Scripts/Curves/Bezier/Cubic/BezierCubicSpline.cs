﻿using System;
using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Cryptography;

[System.Serializable]
public class SplineInfo
{
    public Vector3[] points;
};

[System.Serializable]
public enum BezierControlPointMode
{
    Aligned,
    Mirrored,
    Free
}

public class BezierCubicSpline : MonoBehaviour, ISerializable {
    public int CurveCount
    {
        get { return (points.Length - 1)/3; }
    }

    public int ControlPointCount
    {
        get { return points.Length; }
    }

    [SerializeField] private bool loop;

    public bool Loop
    {
        get
        {
            return loop;
        }
        set
        {
            loop = value;
            if (value == true)
            {
                modes[modes.Length - 1] = modes[0];
                SetControlPoint(0, points[0]);
            }
        }
    }

    public SplineInfo GetSplineInfo()
    {
        SplineInfo splineInfo = new SplineInfo();
        splineInfo.points = points;
        return splineInfo;
    }

    [SerializeField]
    private Vector3[] points;

    [SerializeField] private Matrix4x4[] rotations;

    [SerializeField] private BezierControlPointMode[] modes;

    public int numberOfSteps = 10;
    public bool showCurve = true;
    public bool showDirections = false;
    public bool showAxees = true;

    public int selectedIndex = -1;

    public void Reset()
    {
        points = new Vector3[]
        {
            new Vector3(0f,0f,0f),
            new Vector3(0f,0f,5f),
            new Vector3(0f,0f,10f),
            new Vector3(0f,0f,15f),
        };

        rotations = new Matrix4x4[]
        {
            Matrix4x4.identity, 
            Matrix4x4.identity
        };

        modes = new BezierControlPointMode[]
        {
            BezierControlPointMode.Aligned,
            BezierControlPointMode.Aligned
        };
    }

    public Matrix4x4 GetPoint(float t)
    {
        
        int i;
        Matrix4x4 trans = GetRotation(t);
        if (t >= 1)
        {
            t = 1f;
            i = points.Length - 4;
        }
        else
        {
            t = Mathf.Clamp01(t)*CurveCount;
            i = (int)t;
            t -= i;
            i *= 3;
        }

        
        Vector3 pos = transform.TransformPoint(Bezier.GetPoint(points[i], points[i + 1], points[i + 2], points[i + 3], t));

        trans.SetColumn(3, pos);

        return trans;
    }

    private Matrix4x4 GetRotation(float t)
    {
        Vector3 zDir = GetDirection(t);

        Matrix4x4 returnTransform = new Matrix4x4();
        returnTransform.SetColumn(2, zDir);

        Vector3 localX = Vector3.Cross(Vector3.up, zDir);
        returnTransform.SetColumn(0,localX);

        Vector3 localY = Vector3.Cross(zDir, localX);
        returnTransform.SetColumn(1, localY);

        return returnTransform;
    }


    public Vector3 GetControlPoint(int index)
    {
        return points[index];
    }

    public void SetControlPoint(int index, Vector3 point)
    {
        if (index%3 == 0)
        {
            Vector3 delta = point - points[index];

            if (loop == true)
            {
                if (index == 0)
                {
                    points[1] += delta;
                    points[points.Length - 2] += delta;
                    points[points.Length - 1] = point;
                }
                else if (index == points.Length - 1)
                {
                    points[0] = point;
                    points[1] += delta;
                    points[index - 1] += delta;
                }
                else
                {
                    points[index - 1] += delta;
                    points[index + 1] += delta;
                }
            }
            else
            {
                if (index > 0)
                {
                    points[index - 1] += delta;
                }
                if (index + 1 < points.Length)
                {
                    points[index + 1] += delta;
                }
            }
            
        }

        points[index] = point;

        EnforceMode(index);
    }

    private void EnforceMode(int index)
    {
        int modeIndex = (index + 1)/3;

        BezierControlPointMode mode = modes[modeIndex];

        if (mode == BezierControlPointMode.Free || loop == false && (modeIndex == 0 || modeIndex == modes.Length - 1))
        {
            return;
        }

        int middelIndex = modeIndex*3;
        int fixedIndex, enforcedIndex;

        if (index <= middelIndex)
        {
            fixedIndex = middelIndex - 1;

            if (fixedIndex < 0)
            {
                fixedIndex = points.Length - 2;
            }

            enforcedIndex = middelIndex + 1;

            if (enforcedIndex > points.Length - 1)
            {
                enforcedIndex = 1;
            }
        }
        else
        {
            fixedIndex = middelIndex + 1;

            if (fixedIndex > points.Length - 1)
            {
                fixedIndex = 1;
            }

            enforcedIndex = middelIndex - 1;

            if (enforcedIndex < 0)
            {
                enforcedIndex = points.Length - 2;
            }
        }

        Vector3 middle = points[middelIndex];
        Vector3 enforcedTangent = middle - points[fixedIndex];

        if (mode == BezierControlPointMode.Aligned)
        {
            enforcedTangent = enforcedTangent.normalized*Vector3.Distance(middle, points[enforcedIndex]);
        }
        
        points[enforcedIndex] = middle + enforcedTangent;




    }

    public Vector3 GetVelocity(float t)
    {
        int i;
        if (t >= 1f)
        {
            t = 1f;
            i = points.Length - 4;
        }
        else
        {
            t = Mathf.Clamp01(t) * CurveCount;
            i = (int)t;
            t -= i;
            i *= 3;
        }

        return transform.TransformPoint(Bezier.GetDerivate(points[i + 0], points[i + 1], points[i + 2], points[i + 3], t)) - transform.position;
    }

    public Vector3 GetDirection(float t)
    {
        return GetVelocity(t).normalized;
    }

    public BezierControlPointMode GetControlPointMode(int index)
    {
        return modes[(index + 1)/3];
    }

    public void SetControlPointMode(int index, BezierControlPointMode mode)
    {
        int modeIndex = (index + 1)/3;
        modes[modeIndex] = mode;

        if (loop)
        {
            if (modeIndex == 0)
            {
                modes[modes.Length - 1] = mode;
            }
            else if (modeIndex == modes.Length - 1)
            {
                modes[0] = mode;
            }
        }

        EnforceMode(index);
    }

    public void AddCurve()
    {
        Vector3 point = points[points.Length - 1];
        Vector3 dir = point - points[points.Length - 2];
        dir.Normalize();
        Array.Resize(ref points, points.Length + 3);

        
        const float increment = 5;
        points[points.Length - 3] = point + dir * increment;

        points[points.Length - 2] = point + dir * increment * 2;

        points[points.Length - 1] = point + dir * increment * 3;

        Array.Resize(ref modes, modes.Length + 1);
        EnforceMode(points.Length - 4);

        Array.Resize(ref rotations, rotations.Length + 1);
        rotations[rotations.Length - 1] = rotations[rotations.Length - 2];

        if (loop == true)
        {
            points[points.Length - 1] = points[0];
            modes[modes.Length - 1] = modes[0];
            EnforceMode(0);
        }
    }

    public Quaternion GetControlRotation(int index)
    {
        Matrix4x4 ponintRotation = rotations[(index + 1)/3];

        Quaternion rotation = Matrix44ToQuaternion(ponintRotation);



        return rotation;
    }

    private Quaternion Matrix44ToQuaternion(Matrix4x4 matrix)
    {
        Quaternion rotation = new Quaternion();

        rotation.w = Mathf.Sqrt(Mathf.Max(0, 1 + matrix[0,0] + matrix[1,1] + matrix[2,2])) / 2f;
        rotation.x = Mathf.Sqrt(Mathf.Max(0, 1 + matrix[0, 0] - matrix[1, 1] - matrix[2, 2])) / 2f;
        rotation.y = Mathf.Sqrt(Mathf.Max(0, 1 - matrix[0, 0] + matrix[1, 1] - matrix[2, 2])) / 2f;
        rotation.z = Mathf.Sqrt(Mathf.Max(0, 1 - matrix[0, 0] - matrix[1, 1] + matrix[2, 2])) / 2f;

        rotation.x *= Mathf.Sign(rotation.x*(matrix[2, 1] - matrix[1, 2]));
        rotation.y *= Mathf.Sign(rotation.y*(matrix[0,2] - matrix[2,0]));
        rotation.z *= Mathf.Sign(rotation.z*(matrix[1, 0] - matrix[0, 1])); 

        return rotation;
    }

    public void SetControlRotation(int index, Quaternion newRotation)
    {
        Matrix4x4 newMatrix = Matrix4x4.TRS(Vector3.zero, newRotation, Vector3.one);
        rotations[(index + 1)/3] = newMatrix;
    }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.AddValue("loop", loop);
        info.AddValue("points", points);
        info.AddValue("rotations", rotations);
    }
}
