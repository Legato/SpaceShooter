﻿using UnityEngine;
using System.Collections;

public class BezierCubicCurve : MonoBehaviour
{
    public Vector3[] points;
    public int numberOfSteps = 10;
    public bool showDirections = false;

    public void Reset()
    {
        points = new Vector3[]
        {
            new Vector3(0f,0f,0f),
            new Vector3(5f,0f,0f),
            new Vector3(10f,0f,0f),
            new Vector3(15,0,0), 
        };
    }

    public Vector3 GetPoint(float aInterpolation)
    {
        return transform.TransformPoint(Bezier.GetPoint(points[0], points[1], points[2], points[3], aInterpolation));
    }

    public Vector3 GetVelocity(float t)
    {
        return transform.TransformPoint(Bezier.GetDerivate(points[0], points[1], points[2], points[3], t)) - transform.position;
    }

    public Vector3 GetDirection(float t)
    {
        return GetVelocity(t).normalized;
    }
}
