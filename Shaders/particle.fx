#include "common.fx"
Texture2D shaderTexture : register(t0);
SamplerState samplerState;

struct VertexInput
{
	float4 Position : POSITION;
	float Size : SIZE;
	float Alpha : ALPHA;
	float2 Empty : EMPTY;
};

struct GeometryInput
{
	float4 Position : SV_POSITION;
	float Size : SIZE;
	float Alpha : ALPHA;
	float2 Empty : EMPTY;
};

struct PixelInput
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD;
	float Alpha : ALPHA;
	float Empty : EMPTY;
};

GeometryInput VS_Shader(VertexInput input)
{
	GeometryInput output = (GeometryInput)0;

	output.Position = input.Position;
	//output.Position = mul(particleEmitterInstance.transformation, output.Position);
	output.Position = mul(cameraPositionInv, output.Position);
	output.Size = input.Size;
	output.Alpha = input.Alpha;
	return output;
}

[maxvertexcount(4)]
void GS_Shader(point GeometryInput input[1], inout TriangleStream<PixelInput> triStream)
{
	const float4 offset[4] =
	{
		{-input[0].Size, input[0].Size, 0, 0},
		{ input[0].Size, input[0].Size, 0, 0 },
		{ -input[0].Size, -input[0].Size, 0, 0 },
		{ input[0].Size, -input[0].Size, 0, 0 }
	};
	const float2 uv_coordinates[4] = 
	{
		{0,1},
		{1,1},
		{0,0},
		{1,0}
	};
	for (int i = 0; i < 4; ++i)
	{
		PixelInput vertex = (PixelInput)0;
		vertex.Position = input[0].Position + offset[i];
		float4x4 projectionMatrix = projection;
		vertex.Position = mul(projectionMatrix, vertex.Position);

		vertex.TexCoord = uv_coordinates[i];
		vertex.Alpha = input[0].Alpha;
		triStream.Append(vertex);
	}

	triStream.RestartStrip();
}

float4 PS_Shader(PixelInput input) : SV_TARGET
{
	float2 tex;
	tex.x = input.TexCoord.x;
	tex.y = input.TexCoord.y;
	float4 textureColor = shaderTexture.Sample(samplerState, tex);
	return textureColor;
}