#include "common.fx"

#define TDiffuse 0
#define TSpecular 1
#define TAmbient 2
#define TEmissive 3
#define THeight 4
#define TNormal 5
#define TShininess 6
#define TOpacity 7
#define TDisplacement 8
#define TLightMap 9
#define TReflexion 10
#define TNumOfTextures 11

struct PS_INPUT
{
	float4 position : SV_POSITION;
	float4 colour : COLOR;
	float4 uv : TEXCOORD0;
	float4 normal : TEXCOORD1;
	float4 tangent : TEXCOORD2;
	float4 biNormal : TEXCOORD3;
	float4 viewDirection : TEXCOORD4;
	float4 worldPos : TEXCOORD5;
	float4 viewPosition : VIEWPOSITION;
	float2 specularity : SPECULARITY;
};

struct Pixel_Output
{
	float4 colour : SV_TARGET;
};

Texture2D inTexture[TNumOfTextures] :register(t0);
TextureCube cubemap : register(t11);

SamplerState SampleType;

Pixel_Output SampleTexture(PS_INPUT input, int aTextureType)
{
	Pixel_Output output;
	output.colour = inTexture[aTextureType].Sample(SampleType, input.uv.xy);
	return output;
}

Pixel_Output NormalFromMap(PS_INPUT input)
{
	float3 normal = inTexture[TNormal].Sample(SampleType, input.uv.xy).rgb;

	normal = normalize(normal * float3(2.f, 2.f, 2.f) - float3(1.f, 1.f, 1.f));

	float3x3 normalTransform = float3x3((float3)input.tangent, (float3)input.biNormal, (float3)input.normal);

	normal = mul(normal, normalTransform);

	Pixel_Output output;
	output.colour.rgb = normal;
	output.colour.a = 1.0f;

	return output;
}

Pixel_Output PixelShader_SkyBox(PS_INPUT input)
{
	float3 ambientLight = cubemap.SampleLevel(SampleType, input.viewDirection.xyz, 0).xyz;

	Pixel_Output output;
	output.colour.xyz = ambientLight;
	output.colour.a = 1.0f;
	return output;
}

Pixel_Output Pixel_Shader_Emissive(PS_INPUT aInput)
{
	Pixel_Output output;

	output.colour.xyz = inTexture[TEmissive].Sample(SampleType, aInput.uv.xy).xyz;
	output.colour.a = 1.0f;
	return output;
}

Pixel_Output PixelShader_MetalnessAlbedo(PS_INPUT aInput, float3 albedo, float3 metalness)
{
	float3 metalnessAlbedo = albedo - (albedo * metalness);

	Pixel_Output output;
	output.colour.rgb = metalnessAlbedo.rgb;
	output.colour.a = 1.0f;
	return output;
}

Pixel_Output PixelShader_Substance(PS_INPUT aInput, float3 albedo, float3 metalness)
{
	float3 substance = (float3(0.04f, 0.04f, 0.04f) - (float3(0.04f, 0.04f, 0.04f) * metalness)) + albedo * metalness;

	Pixel_Output output;
	output.colour.rgb = substance.rgb;
	output.colour.a = 1.0f;
	return output;
}

Pixel_Output PixelShader_ReflectionFresnel(PS_INPUT aInput, float roughness, float3 normal, float3 toEyeVector, float3 substance)
{
	float VdotN = dot(toEyeVector.xyz, normal.xyz);
	VdotN = saturate(VdotN);
	VdotN = 1.f - VdotN;
	VdotN = pow(VdotN, 5);

	float3 fresnel = VdotN * (1.f - substance);
	fresnel = fresnel / (6 - 5 * roughness);
	fresnel = substance + fresnel;

	Pixel_Output output;
	output.colour.xyz = fresnel;
	output.colour.a = 1.f;

	return output;
}

Pixel_Output PixelShader_Roughness(PS_INPUT input)
{
	Pixel_Output output;
	output.colour.xyz = inTexture[TSpecular].Sample(SampleType, input.uv.xy).xyz;
	output.colour.a = 1.f;
	return output;
}

static const float minRoughness = 0.0014;

float RoughToSPow(float fRoughness)
{
	min(fRoughness, minRoughness);

	return (2.f / (fRoughness * fRoughness)) - 2.f;
}

static const float k0 = 0.00098f;
static const float k1 = 0.9921f;
static const float fakeLysMaxSpecularPower = (2.f / (minRoughness * minRoughness)) - 2.f;
static const float fMaxT = (exp2(-10.f / sqrt(fakeLysMaxSpecularPower)) - k0) / k1;

float GetSpecPowToMip(float fSpecPow, int nMips)
{
	float fSmulMaxT = (exp2(-10.0f / sqrt(fSpecPow)) - k0) / k1;

	return float(nMips - 1 - 0) * (1.f - clamp(fSmulMaxT / fMaxT, 0.0f, 1.0f));
}

Pixel_Output PixelShader_Lambert(PS_INPUT input, float3 normal, float3 toLightVector)
{
	float TdotN = saturate(dot(toLightVector, normal));
	float3 lambert = TdotN.xxx;

	Pixel_Output output = (Pixel_Output)0;
	output.colour.rgb = lambert;
	output.colour.a = 1.f;
	return output;
} 

Pixel_Output PixelShader_Fresnel(PS_INPUT input, uint aLightIndex, float3 toLightVector, float3 toEyeVector, float3 substance)
{
	float3 halfVec = normalize(toLightVector + toEyeVector);

	float LdotH = dot(toLightVector, halfVec);
	LdotH = saturate(LdotH);
	LdotH = 1.f - LdotH;
	LdotH = pow(LdotH, 5);
	float3 fresnel = LdotH * (1.f - substance);
	fresnel = substance + fresnel;

	Pixel_Output output = (Pixel_Output)0;
	output.colour.rgb = fresnel.xyz;
	output.colour.a = 1.f;
	return output;
}

Pixel_Output PixelShader_Distribution(PS_INPUT input, float3 toLight, float roughness, float3 normal, float3 toEye)
{
	float3 halfVec = normalize(toLight + toEye);
	float HdotN = saturate(dot(halfVec, normal));

	float m = roughness * roughness;
	float m2 = m * m;

	float denominator = (HdotN * HdotN) * (m2 - 1.f) + 1;
	float distribution = m2 / (3.14159 * denominator * denominator);

	Pixel_Output output = (Pixel_Output)0;
	output.colour.rgb = distribution.xxx;
	output.colour.a = 1.f;
	return output;
}

Pixel_Output PixelShader_Visibility(PS_INPUT input, float3 toLightVector, float roughness, float3 normal, float3 toEyeVector)
{
	float roughnessRemapped = (roughness + 1.f) / 2.f;

	float NdotL = saturate(dot(normal, toLightVector));
	float NdotV = saturate(dot(normal, toEyeVector));

	float k = roughnessRemapped * roughnessRemapped * 1.7724f;
	float G1V = NdotV * (1.f - k) + k;
	float G1L = NdotL * (1.f - k) + k;
	float visibility = (NdotV * NdotL) / (G1V * G1L);

	Pixel_Output output = (Pixel_Output)0;
	output.colour.xyz = visibility.xxx;
	output.colour.a = 1.f;
	return output;
}

Pixel_Output PixelShader_AmbientDiffuse(PS_INPUT aInput, float3 normal, float3 metalnessAlbedo, float3 ambientOcclusion, float3 fresnel)
{
	float3 ambientLight = cubemap.SampleLevel(SampleType, normal.xyz, 10).xyz;

	Pixel_Output output;
	output.colour.xyz = metalnessAlbedo * ambientLight * ambientOcclusion * (float3(1.f, 1.f, 1.f) - fresnel) * ambientDiffuseIntensity;
	output.colour.a = 1.f;

	return output;
}

Pixel_Output PixelShader_AmbientSpecular(PS_INPUT input, float3 ambientOcclusion, float3 reflectionVector, float3 fresnel, float roughness)
{
	float fakeLysSpecularPower = RoughToSPow(roughness);
	float lysMipMap = GetSpecPowToMip(fakeLysSpecularPower, uint(12));
	float3 ambientLight = cubemap.SampleLevel(SampleType, reflectionVector.xyz, lysMipMap).xyz;

	Pixel_Output output;
	output.colour.rgb = ambientLight * ambientOcclusion * fresnel * ambientSpecIntensity;
	output.colour.a = 1.f;
	return output;
}

Pixel_Output PixelShader_DirectDiffuse(PS_INPUT input, float3 lightColor, float3 metalnessAlbedo, float3 lambert, float3 fresnel)
{

	Pixel_Output output = (Pixel_Output)0;
	output.colour.rgb = metalnessAlbedo.rgb * lightColor * lambert.rgb * (float3(1.f, 1.f, 1.f) - fresnel);
	output.colour.a = 1.f;

	return output;
}

Pixel_Output PixelShader_DirectSpecularity(PS_INPUT input, float3 normal, float3 fresnel, float3 lambert, float3 lightColor, float roughness, float3 toEyeVector, float3 toLightVector)
{
	float3 distribution = PixelShader_Distribution(input, toLightVector, roughness, normal, toEyeVector).colour.rgb;
	float3 visibility = PixelShader_Visibility(input, toLightVector, roughness, normal, toEyeVector).colour.rgb;

	Pixel_Output output = (Pixel_Output)0;
	output.colour.rgb = lightColor * fresnel * lambert * distribution * visibility;
	output.colour.a = 1.f;

	return output;
}

Pixel_Output Frag_Pos(PS_INPUT aInput)
{
	float3 normal = NormalFromMap(aInput).colour.rgb;
	float3 albedo = SampleTexture(aInput, TDiffuse).colour.rgb;
	float3 metalness = SampleTexture(aInput, TReflexion).colour.rgb;
	float3 metalnessAlbedo = PixelShader_MetalnessAlbedo(aInput, albedo, metalness).colour.rgb;
	float3 substance = PixelShader_Substance(aInput, albedo, metalness).colour.rgb;
	float3 ambientOcclusion = SampleTexture(aInput, TAmbient).colour.xyz;
	float roughness = PixelShader_Roughness(aInput).colour.x;
	float3 toEyeVector = normalize(aInput.viewPosition.xyz - aInput.worldPos.xyz);
	float3 reflectionVector = -reflect(toEyeVector, normal);
	float3 reflectionFresnel = PixelShader_ReflectionFresnel(aInput, roughness, normal, toEyeVector, substance).colour.rgb;

	float3 emissiveOut = Pixel_Shader_Emissive(aInput).colour.rgb;
	float3 ambientDiffuse = PixelShader_AmbientDiffuse(aInput, normal, metalnessAlbedo, ambientOcclusion, reflectionFresnel).colour.rgb;
	float3 ambientOcclusionSpec = ambientOcclusion.xxx;
	float3 ambientSpecular = PixelShader_AmbientSpecular(aInput, ambientOcclusionSpec, reflectionVector, reflectionFresnel, roughness).colour.rgb;

	float3 directDiffuse = float3(0.f,0.f,0.f);
	float3 directSpecular = float3(0.f,0.f,0.f);
	for (uint i = 0; i < numberOfDirectionalLights; ++i)
	{
		
		float3 lightColor = directionalColors[i].xyz;
		float3 toLightVector = -normalize(directionalLightDirections[i].rgb);
		float3 fresnel = PixelShader_Fresnel(aInput, i, toLightVector, toEyeVector, substance).colour.xyz;
		float3 lambert = PixelShader_Lambert(aInput, normal, toLightVector).colour.xyz;
		
		directSpecular += PixelShader_DirectSpecularity(aInput, normal, fresnel, lambert, lightColor, roughness, toEyeVector, toLightVector).colour.rgb;
		directDiffuse += PixelShader_DirectDiffuse(aInput, lightColor, metalnessAlbedo, lambert, fresnel).colour.rgb;
	}
	saturate(directSpecular);
	saturate(directDiffuse);
	Pixel_Output output;

	output.colour.rgb =  emissiveOut + ambientSpecular + ambientDiffuse + directSpecular + directDiffuse;
	output.colour.a = 1.0f;

	return output;
}

float4 PixelShader_Skybox(PS_SKYBOX_INPUT input) : SV_TARGET
{
	float3 toPixel = normalize(input.worldPos.xyz);
	float3 ambientLight = cubemap.SampleLevel(SampleType, toPixel.xyz, 0).xyz;

	float4 output;
	output.xyz = ambientLight;
	output.a = 1.0f;
	return output;
}