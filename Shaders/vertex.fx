#include "common.fx"

struct VS_INPUT
{
	float4 position : POSITION;
	float4 color : COLOR;
	float4 uv : TEXCOORD0;
	float4 normal : TEXCOORD1;
	float4 tangent : TEXCOORD2;
	float4 biTangent : TEXCOORD3;
	float4 bone : TEXCOORD4;
	float4 weight : TEXCOORD5;
	//instance fields
	/*float2 specularity : SPECULARITY;
	float2 empty : TEXCOORD6;
	float4x4 toWorld : TRANSFORMMATRIX;*/
};

struct PS_INPUT
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float4 uv : TEXCOORD0;
	float4 normal : TEXCOORD1;
	float4 tangent : TEXCOORD2;
	float4 biNormal : TEXCOORD3;
	float4 viewDirection : TEXCOORD4;
	float4 worldPos : TEXCOORD5;
};


PS_SKYBOX_INPUT Vertex_SkyBox(VS_SKYBOX_INPUT input)
{
	PS_SKYBOX_INPUT output;
	output.position = input.position;
	output.position.w = 1.f;

	float4x4 instanceTransform = instance.transformation;
	output.worldPos = mul(instanceTransform, output.position);

	float3x3 cameraOrientation = (float3x3)cameraPositionInv;

	output.position.xyz = mul(cameraOrientation, output.position.xyz);
	output.position = mul(projection, output.position);
	return output;
}

PS_INPUT Vertex_Pos(VS_INPUT input)
{
	PS_INPUT output;

	output.position = input.position;
	output.position.w = 1.f;
	
	float4x4 instanceTransform = instance.transformation;

	output.position = mul(instanceTransform, output.position);

	output.worldPos = output.position;

	output.position = mul(cameraPositionInv, output.position);
	output.position = mul(projection, output.position);
	//output.position = mul(input.toWorld, output.position);
	output.color = input.color;
	output.uv = input.uv;

	matrix rotation  = instance.transformation;
	
	output.normal = float4(normalize(
		mul(
		(float3x3)rotation,
		(float3)input.normal
		))
	,1.f);

	output.tangent = float4(normalize(
		mul(
		(float3x3)rotation,
			(float3)input.tangent
		))
		, 1.f);

	output.biNormal = float4(normalize(
		mul(
		(float3x3)rotation,
			(float3)input.biTangent
		))
		, 1.f);


	float4 worldPosition;
	float3 cameraPosition = (float3)cameraPosVector;



	worldPosition = mul(instance.transformation, input.position);
	
	output.viewDirection = float4(normalize(cameraPosition - worldPosition.xyz), 1.f);
	
	//output.specularity = input.specularity;
	
	return output;
}