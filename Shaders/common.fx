struct VS_SKYBOX_INPUT
{
	float4 position : POSITION;
};

struct PS_SKYBOX_INPUT
{
	float4 position : SV_POSITION;
	float4 worldPos : TEXCOORD0;
};

cbuffer projection : register(b0)
{
	matrix projection;
};

cbuffer cameraPos : register(b1)
{
	matrix cameraPositionInv;
	float4 cameraPosVector;
};

cbuffer instanceData : register(b2)
{
	struct
	{
		float4x4 transformation;
	} instance;
	
};

#define MAX_NUM_LIGHTS 8
cbuffer staticLighting : register(b3)
{
	float4 directionalLightDirections[MAX_NUM_LIGHTS];
	float4 directionalColors[MAX_NUM_LIGHTS];
	uint numberOfDirectionalLights;
	float ambientDiffuseIntensityNotReal;
	float ambientSpecIntensityNotReal;
	float empty;
};

cbuffer particleData : register(b4)
{
	struct
	{
		float4x4 transformation;
	}particleEmitterInstance;
};

cbuffer skyboxData : register(b5)
{
	float ambientDiffuseIntensity;
	float ambientSpecIntensity;
	float2 padding;
};