﻿using UnityEngine;
using System.Collections;

public class RotateAsteroid : MonoBehaviour
{
    public float speed = 15f;
	void Update ()
    {
        transform.Rotate(Vector3.right * speed * Time.deltaTime);
        transform.Rotate(Vector3.up * speed * Time.deltaTime);
        transform.Rotate(Vector3.forward * speed * Time.deltaTime);

	}

}
